import styled from 'styled-components'
import React, { useState } from 'react'
import Head from 'next/head'

import { Collapse } from 'antd'

import Header from '../components/Header'
import Footer from '../components/Footer'

export default function Home ({ children }) {
	// state = { visible: false, placement: 'left' };

	return (
		<Wrapper>
			<Head>
				<title>TRICOTS </title>
			</Head>
			<Header />
				<div style={{paddingTop : "13vh"}}>{children}</div>	
			<Footer />
		</Wrapper>
	)
}

const Wrapper = styled.section`
	font-family: "Montserrat", sans-serif;
	width: 100vw;
	width: 100%;
`
