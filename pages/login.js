import styled from "styled-components";
import Themes from "../themes/themes";
import React, { useEffect, useState } from "react";
import Head from "next/head";
import { Form, Input, Button, Checkbox, message } from "antd";
import {
	UserOutlined,
	LockOutlined,
	GoogleCircleFilled,
	GoogleOutlined,
} from "@ant-design/icons";
import { useDispatch, useSelector } from "react-redux";
import { useRouter } from "next/router";

import Layout from "../layouts/HomeLayout";
import BreadCrumb from "../components/BreadCrumb";
import AuthActions from "../redux/actions/auth";
import GoogleLogin from "react-google-login";
import Images from "../assests/index";
export default function Login() {
	const router = useRouter();
	const dispatch = useDispatch();
	const [username, setUsername] = useState("");
	const [password, setPassword] = useState("");
	const [firstName, setFirstName] = useState("");
	const [lastName, setLastName] = useState("");

	const {
		error,
		loggingIn,
		logInSuccessful,
		signingUp,
		signupSuccessful,
		user,
	} = useSelector((state) => state.auth);

	useEffect(() => {
		dispatch(AuthActions.clearError());
	}, []);
	useEffect(() => {
		let token = localStorage.getItem("accessToken")
		if(token) {
			message.success("Already Logged In, Enjoy Shopoing")
			router.push("/")
		}
	}, []);

	useEffect(() => {
		dispatch(
			AuthActions.stateReset({
				logInSuccessful: false,
				logInSuccessful: false,
				signupSuccessful: false,
				signingUp: false,
			})
		);
	}, []);


	useEffect(() => {
		if (error === "Please check your username and/or password.") {
			console.log(error);
			dispatch(
				AuthActions.signupRequest({
					emailId: username,
					password: password,
					confirm: password,
					firstName: firstName,
					lastName: lastName,
				})
			);
		} else if (error) {
			message.error(error);
			dispatch(AuthActions.clearError());
		}
	}, [error]);

	useEffect(() => {
		if (!signingUp && signupSuccessful) {
			dispatch(AuthActions.stateReset({
				signupSuccessful: false
			}))
			dispatch(AuthActions.loginRequest(username, password))
			message.success('Registered Successfully')
		}
	}, [signupSuccessful])

	useEffect(() => {
		if (!loggingIn && logInSuccessful) {
			dispatch(AuthActions.stateReset({
				logInSuccessful: false
			}))
			router.back()
			message.success('Logged in Successfully')
		}
	}, [logInSuccessful])

	const onFinish = ({ username, password }) => {
		dispatch(AuthActions.loginRequest(username, password));
	};
	const responseGoogle = (response) => {
		setUsername(response?.profileObj?.email);
		dispatch(AuthActions.loginRequest(username, "12345678"));
		setPassword("12345678");
		setFirstName(response?.profileObj?.givenName);
		setLastName(response?.profileObj?.familyName);

		dispatch(AuthActions.loginRequest(response?.profileObj?.email, "12345678"));

	};

	return (
		<Wrapper>
			<Head>
				<title>TRICOTS </title>
			</Head>
			<Layout>
				<BreadCrumb data={[{ name: "Home", route: "/" }, { name: "Login" }]} />
				<div className="container">
					<p>LOGIN</p>
					<Form
						name="normal_login"
						className="login-form"
						initialValues={{
							remember: true,
						}}
						onFinish={onFinish}
					>
						<Form.Item
							name="username"
							rules={[
								{
									required: true,
									message: "Please input your Username!",
								},
							]}
						>
							<Input
								prefix={<UserOutlined className="site-form-item-icon" />}
								placeholder="Email"
							/>
						</Form.Item>
						<Form.Item
							name="password"
							rules={[
								{
									required: true,
									message: "Please input your Password!",
								},
							]}
						>
							<Input
								prefix={<LockOutlined className="site-form-item-icon" />}
								type="password"
								placeholder="Password"
							/>
						</Form.Item>
						<Form.Item>
							<Form.Item name="remember" valuePropName="checked" noStyle>
								<Checkbox>Remember me</Checkbox>
							</Form.Item>

							{/* <a className="login-form-forgot" href="">
								Forgot password
							</a> */}
						</Form.Item>

						<Form.Item>
							<Button
								loading={loggingIn}
								type="primary"
								htmlType="submit"
								className="login-form-button"
							>
								Log in
							</Button>
							Or <a href="/signup">register now!</a>
						</Form.Item>
						<GoogleLogin
							render={(renderProps) => (
								<Button
									className="login-form-button"
									icon={
										<img
											style={{
												width: 20,
												height: 20,
												marginRight: 20,
												objectFit: "contain",
											}}
											src={Images.google}
										/>
									}
									onClick={renderProps.onClick}
									disabled={renderProps.disabled}
								>
									Login with Google
								</Button>
							)}
							disabledStyle={{ backgroundColor: "red" }}
							isSignedIn={true}
							// jsSrc={"https://apis.google.com/js/api.js"}
							clientId="711691814112-tepb25r7b4dj2itsuhlumtiuehtbjflg.apps.googleusercontent.com"
							buttonText="Login with Google"
							onSuccess={responseGoogle}
							onFailure={responseGoogle}
							cookiePolicy={"single_host_origin"}
						></GoogleLogin>
					</Form>
				</div>
			</Layout>
		</Wrapper>
	);
}

const Wrapper = styled.section`
	font-family: "Montserrat", sans-serif;
	width: 100vw;
	width: 100%;
	.container {
		padding: 20px;
		display: flex;
		justify-content: center;
		align-items: center;
		flex-direction: column;
		/* background-color: ${Themes.backgroundMain}; */
	}
	.login-form {
		max-width: 350px;
	}
	.login-form-forgot {
		float: right;
	}
	.ant-col-rtl .login-form-forgot {
		float: left;
	}
	.login-form-button {
		width: 100%;
	}
	.login-form-button {
		width: 100%;
	}
`;
