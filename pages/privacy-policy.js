import styled from "styled-components";
import React, { useEffect } from "react";
import Head from "next/head";

import Layout from "../layouts/HomeLayout";

export default function Signup() {
	const admin = JSON.parse(localStorage.getItem("admin"))
	return (
		<Wrapper>
			<Head>
				<title>TRICOTS </title>
			</Head>
			<Layout>
			<pre>
				{admin?.privacyPolicy}
			</pre>
			</Layout>
		</Wrapper>
	);
}

const Wrapper = styled.section`
	font-family: "Montserrat", sans-serif;
	width: 100vw;
	width: 100%;
	.container {
		padding: 7px;
	}
`;
