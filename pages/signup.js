import styled from 'styled-components'
import Themes from '../themes/themes'
import React, { useEffect, useState } from 'react'
import Head from 'next/head'
import { Form, Input, Button, Checkbox, message } from 'antd'
import { UserOutlined, LockOutlined, MailOutlined } from '@ant-design/icons'
import { useDispatch, useSelector } from 'react-redux'
import { useRouter } from 'next/router'

import Layout from '../layouts/HomeLayout'
import BreadCrumb from '../components/BreadCrumb'
import AuthActions from '../redux/actions/auth'

export default function Signup () {
	const router = useRouter()
	const dispatch = useDispatch()
	const { error, signingUp, signupSuccessful, user, loggingIn, logInSuccessful } = useSelector(state => state.auth)

	const [username, setUsername] = useState('')
	const [password, setPassword] = useState('')

	useEffect(() => {
		dispatch(AuthActions.stateReset({
			signupSuccessful: false,
			signingUp: false,
			logInSuccessful: false,
			logInSuccessful: false
		}))
	}, [])

	useEffect(() => {
		if (error) {
			message.error(error)
			dispatch(AuthActions.clearError())
		}
	}, [error])

	useEffect(() => {
		if (!signingUp && signupSuccessful) {
			dispatch(AuthActions.stateReset({
				signupSuccessful: false
			}))
			dispatch(AuthActions.loginRequest(username, password))
			message.success('Registered Successfully')
		}
	}, [signupSuccessful])

	useEffect(() => {
		if (!loggingIn && logInSuccessful) {
			dispatch(AuthActions.stateReset({
				logInSuccessful: false
			}))
			router.push('/')
			message.success('Logged in Successfully')
		}
	}, [logInSuccessful])

	const onFinish = ({ emailId, firstName, lastName, confirm, password }) => {
		setUsername(emailId)
		setPassword(password)

		if (password !== confirm) {
			return message.warn('Passsword are not matching')
		}

		dispatch(AuthActions.signupRequest({
			emailId: emailId,
			password: password,
			confirm: confirm,
			firstName: firstName,
			lastName: lastName
		}))
	}

	return (
		<Wrapper>
			<Head>
				<title>TRICOTS </title>
			</Head>
			<Layout>
            <BreadCrumb
					data={[{ name: 'Home', route: '/' }, { name: 'Register' }]}
				/>
				<div className="container">
					<p>REGISTER</p>
					<Form
						name="normal_register"
						className="register-form"
						initialValues={{
							remember: true
						}}
						onFinish={onFinish}
					>
						<Form.Item
							name="firstName"
							rules={[
								{
									required: true,
									message: 'Please input your First Name!'
								}
							]}
						>
							<Input
								prefix={
									<UserOutlined className="site-form-item-icon" />
								}
								placeholder="First Name"
							/>
						</Form.Item>
                        <Form.Item
							name="lastName"
							rules={[
								{
									required: true,
									message: 'Please input your Last Name!'
                                }

							]}
						>
							<Input
								prefix={
									<UserOutlined className="site-form-item-icon" />
								}
								placeholder="Last Name"
							/>
						</Form.Item>
                        <Form.Item
							name="emailId"
							rules={[
								{
									required: true,
                                    message: 'Please input your email!'
                                },
                                {
                                    type: 'email',
                                    message: 'The input is not valid E-mail!'
                                  }
							]}
						>
							<Input
								prefix={
									<MailOutlined className="site-form-item-icon" />
								}
								placeholder="Email"
							/>
						</Form.Item>
						<Form.Item
							name="password"
							rules={[
								{
									required: true,
									message: 'Please input your Password!'
								}
							]}
						>
							<Input
								prefix={
									<LockOutlined className="site-form-item-icon" />
								}
								type="password"
								placeholder="Password"
							/>
						</Form.Item>
						<Form.Item
							name="confirm"
							rules={[
								{
									required: true,
									message: 'Please input your Password!'
								}
							]}
						>
							<Input
								prefix={
									<LockOutlined className="site-form-item-icon" />
								}
								type="password"
								placeholder="Confirm Password"
							/>
						</Form.Item>

						<Form.Item>
							<Button
							loading={signingUp}
								type="primary"
								htmlType="submit"
								className="register-form-button"
							>
								Sign Up
							</Button>
						</Form.Item>
					</Form>
				</div>
			</Layout>
		</Wrapper>
	)
}

const Wrapper = styled.section`
	font-family: "Montserrat", sans-serif;
	width: 100vw;
	width: 100%;
	.container {
		padding: 20px;
		display: flex;
		justify-content: center;
		align-items: center;
		flex-direction: column;
		/* background-color: ${Themes.backgroundMain}; */
	}
	.register-form {
		max-width: 350px;
	}
	.register-form-forgot {
		float: right;
	}
	.ant-col-rtl .register-form-forgot {
		float: left;
	}
	.register-form-button {
		width: 100%;
	}
	.register-form-button {
		width: 100%;
	}
`
