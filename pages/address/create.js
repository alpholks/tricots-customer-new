import styled from 'styled-components'
import Themes from '../../themes/themes'
import React, { useEffect, useState } from 'react'
import Head from 'next/head'
import { Form, Input, Button, Radio, message, Grid, Row, Col } from 'antd'
import { UserOutlined, LockOutlined } from '@ant-design/icons'
import { useDispatch, useSelector } from 'react-redux'
import { useRouter } from 'next/router'

import Layout from '../../layouts/HomeLayout'
import BreadCrumb from '../../components/BreadCrumb'
import ProfileAction from '../../redux/actions/profile'

export default function CreateAddress () {
	const [form] = Form.useForm()

	const router = useRouter()
	const dispatch = useDispatch()

	const screens = Grid.useBreakpoint()
	const { error, creatingAddress, createAddressSuccessful } = useSelector(
		(state) => state.profile
	)
	const onChange = (e) => {
		form.setFieldsValue({
			type: e.target.value
		})
	}
	useEffect(() => {
		dispatch(ProfileAction.clearError())
	}, [])

	useEffect(() => {
		dispatch(
			ProfileAction.stateReset({
				creatingAddress: false,
				createAddressSuccessful: false
			})
		)
	}, [])

	useEffect(() => {
		if (error) {
			message.error(error)
		}
	}, [error])

	useEffect(() => {
		if (!creatingAddress && createAddressSuccessful) {
			message.success('Address created successfully')
			dispatch(
				ProfileAction.stateReset({
					createAddressSuccessful: false
				})
			)
			router.back()
		}
	}, [createAddressSuccessful])

	const hanldeSaveAddress = ({
		name,
		type,
		doorNumber,
		street,
		landmark,
		district,
		state,
		pincode,
		phoneNumber,
		secondaryPhoneNumber
	}) => {
		dispatch(
			ProfileAction.createAddressRequest({
				name,
				type,
				doorNumber,
				street,
				landmark,
				district,
				state,
				pincode,
				phoneNumber,
				secondaryPhoneNumber
			})
		)
	}

	return (
		<Wrapper view={screens.xs ? 'mobile' : 'web'}>
			<Layout>
				<BreadCrumb
					data={[
						{ name: 'Home', route: '/' },
						{ name: 'Create Address' }
					]}
				/>
				<div className="container">
					<p>Create Address</p>

					<Form
						name="normal_register"
						className="register-form"
						initialValues={{
							remember: true
						}}
						onFinish={hanldeSaveAddress}
					>
						<Row gutter={24} justify="space-between" align="middle">
							<Col xs={24} lg={24}>
								<Form.Item
									name="type"
									rules={[
										{
											required: true,
											message:
												'Please select the address type!'
										}
									]}
								>
									<Radio.Group
										onChange={onChange}
										value={'Home'}
										style={{ width: '100%' }}
									>
										<Row
											justify="space-evenly"
											align="middle"
										>
											<Col xs={24} lg={4}>
												<Radio value={'Home'}>Home</Radio>
											</Col>
											<Col xs={24} lg={4}>
												<Radio value={'Office'}>Office</Radio>
											</Col>
											{/* <Col xs={24} lg={4}>
												<Radio value={3}>
													Guests House
												</Radio>
											</Col> */}
											<Col xs={24} lg={4}>
												<Radio value={'Others'}>Other</Radio>
											</Col>
										</Row>
									</Radio.Group>
								</Form.Item>
							</Col>
							<Col xs={24} lg={24}>
								<Form.Item
									name="name"
									rules={[
										{
											required: true,
											message:
												'Please input your First Name!'
										}
									]}
								>
									<Input placeholder="Name" />
								</Form.Item>
							</Col>

							<Col xs={24} lg={12}>
								<Form.Item
									name="doorNumber"
									rules={[
										{
											required: true,
											message:
												'Please input your door no / falt no!'
										}
									]}
								>
									<Input placeholder="Door number / Flat" />
								</Form.Item>
							</Col>

							<Col xs={24} lg={12}>
								<Form.Item
									name="street"
									rules={[
										{
											required: true,
											message:
												'Please enter street address!'
										}
									]}
								>
									<Input placeholder="Address line 1" />
								</Form.Item>
							</Col>
							<Col xs={24} lg={12}>
								<Form.Item name="landmark">
									<Input placeholder="Address line 2" />
								</Form.Item>
							</Col>
							<Col xs={24} lg={12}>
								<Form.Item
									name="district"
									rules={[
										{
											required: true,
											message: 'Please enter your City!'
										}
									]}
								>
									<Input placeholder="City" />
								</Form.Item>
							</Col>
							<Col xs={24} lg={12}>
								<Form.Item
									name="state"
									rules={[
										{
											required: true,
											message: 'Please enter your State!'
										}
									]}
								>
									<Input placeholder="State" />
								</Form.Item>
							</Col>
							<Col xs={24} lg={12}>
								<Form.Item
									name="pincode"
									rules={[
										{
											required: true,
											message:
												'Please enter your Postal Code!'
										}
									]}
								>
									<Input placeholder="Postal Code" />
								</Form.Item>
							</Col>
							<Col xs={24} lg={12}>
								<Form.Item
									name="phoneNumber"
									rules={[
							
										{
											required: true,
											message:
												'Please enter your Phone Number!'
										}
									]}
								>
									<Input placeholder="Phone Number" />
								</Form.Item>
							</Col>
							<Col xs={24} lg={12}>
								<Form.Item
									name="secondaryPhoneNumber"
									rules={[
							
										{
											required: true,
											message:
												'Please enter your Phone Number!'
										}
									]}
								>
									<Input placeholder="Secondary Phone Number" />
								</Form.Item>
							</Col>
							<Col xs={24} lg={24}>
								<Form.Item>
									<Button
										loading={creatingAddress}
										type="primary"
										htmlType="submit"
										className="register-form-button"
									>
										Save Address
									</Button>
								</Form.Item>
							</Col>
						</Row>
					</Form>
				</div>
			</Layout>
		</Wrapper>
	)
}

const Wrapper = styled.section`
	font-family: "Montserrat", sans-serif;
	width: 100vw;
	width: 100%;

	.container {
		padding: 20px;
		display: flex;
		justify-content: center;
		align-items: center;
		flex-direction: column;
		/* background-color: ${Themes.backgroundMain}; */
	}
	.register-form {
		/* max-width: 350px; */
		width: ${(props) => (props.view === 'mobile' ? '100%' : '60vw')};
		/* width: 100%; */
	}
	.register-form-forgot {
		float: right;
	}
	.ant-col-rtl .register-form-forgot {
		float: left;
	}
	.register-form-button {
		width: 100%;
	}
	.register-form-button {
		width: 100%;
	}
	[data-theme="compact"]
		.site-collapse-custom-collapse
		.site-collapse-custom-panel,
	.site-collapse-custom-collapse1 .site-collapse-custom-panel {
		overflow: hidden;
		background: #ffffff;
		border: 0px;
		border-radius: 2px;
		width: 100vw;
	}
	.back-btn {
		border: 1px solid ${Themes.primary};
		color: ${Themes.primary};
		margin-bottom: 10px;
		margin-top: 20px;
		width: 100%;
		height: 40px;
	}
`
