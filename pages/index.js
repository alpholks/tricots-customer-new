import styled from "styled-components";
import Themes from "../themes/themes";
import React, { useEffect, useState } from "react";
import { message, Grid, Row, Col } from "antd";
import Head from "next/head";
import { DownOutlined } from "@ant-design/icons";
import { useDispatch, useSelector } from "react-redux";
import { useRouter } from "next/router";

import Header from "../components/Header";
import Footer from "../components/Footer";
import ProductCard from "../components/ProductCard/ProductCard";
import Categories from "../components/Categories";
import Collections from "../components/Collections";
import { BannerTop, BannerBottom } from "../components/Banner";
import GeneralActions from "../redux/actions/general";
const { useBreakpoint } = Grid;

export default function Home() {
	const router = useRouter();
	const dispatch = useDispatch();
	const screens = useBreakpoint();

	const {
		error,
		fetchingBanners,
		fetchBannersSuccessful,
		banners,
		fetchingCollections,
		fetchCollectionsSuccessful,
		collections,
		fetchingProducts,
		fetchProductsSuccessful,
		products,
	} = useSelector((state) => state.general);

	useEffect(() => {
		dispatch(GeneralActions.clearError());
	}, []);

	useEffect(() => {
		dispatch(
			GeneralActions.stateReset({
				fetchingBanners: false,
				fetchBannersSuccessful: false,
				fetchingCollections: false,
				fetchCollectionsSuccessful: false,
				fetchingProducts: false,
				fetchProductsSuccessful: false,
			})
		);
	}, []);

	useEffect(() => {
		if (error) {
			message.error(error);
		}
	}, [error]);

	useEffect(() => {
		dispatch(GeneralActions.getBannersRequest());
		dispatch(GeneralActions.getCollectionsRequest());
		dispatch(GeneralActions.getProductsRequest("isNewArrival"))
		// dispatch(GeneralActions.getProductsRequest("bestSale"))
		// dispatch(GeneralActions.getProductsRequest("isFeatured"))
	}, []);

	useEffect(() => {
		if (!fetchingBanners && fetchBannersSuccessful) {
			dispatch(
				GeneralActions.stateReset({
					fetchBannersSuccessful: false,
				})
			);
		}
	}, [fetchBannersSuccessful]);

	useEffect(() => {
		if (!fetchingCollections && fetchCollectionsSuccessful) {
			dispatch(
				GeneralActions.stateReset({
					fetchCollectionsSuccessful: false,
				})
			);
		}
	}, [fetchCollectionsSuccessful]);

	// useEffect(() => {
	// 	if (!fetchingProducts && fetchProductsSuccessful) {
	// 		dispatch(
	// 			GeneralActions.stateReset({
	// 				fetchProductsSuccessful: false,
	// 			})
	// 		);
	// 	}
	// }, [fetchProductsSuccessful]);

	return (
		<Wrapper size={screens.xs ? "mobile" : "web"}>
			<Head>
				<title>TRICOTS </title>
			</Head>
			<Header />
			<div className="main-container">
				<BannerTop
					data={banners?.filter((banner) => {
						if (banner.type === "small") return banner;
					})}
				/>
				<BannerBottom
					data={banners?.filter((banner) => {
						if (banner.type === "large") return banner;
					})}
				/>

				{/* <Categories data={collections.filter((collection, index) => {if(index+1 <=5) return collection})} onClickCollection={(route)=> router.push(route)}/> */}
				<Collections
					data={collections}
					onClickCollection={(route) => router.push(route)}
				/>
					{/* <Row gutter={[0, 12]}>
						{products.map((product) => {
							return (
								<Col xs={12} sm={12} md={12} l={8} xl={6}>
									<ProductCard
										addToWishList={(productId) =>
											// addToWishList(productId)
											{}
										}
										removeFromWishList={(productId) =>
											// removeFromWishList(productId)
											{}
										}
										data={product}
										onClick={() =>
											// handleClickProduct(product)
											{}
										}
									/>
								</Col>
							);
						})}
					</Row> */}
				<div className="happy-customer">
					Over <span className="customer-count"> {JSON.parse(localStorage.getItem("admin"))?.happyCustomers} </span>{" "}
					Happy Customers
				</div>
				<div className="indian-brand">HOMEGROWN INDIAN FASHION LABEL</div>
				<Footer />
			</div>
		</Wrapper>
	);
}

const Wrapper = styled.section`
	font-family: "Montserrat", sans-serif;
	width: 100vw;
	width: 100%;
	.main-container {
		margin-top: 13vh;
	}
	.happy-customer {
		font-size: ${(props) => (props.size === "mobile" ? "18px" : "35px")};
		display: flex;
		justify-content: center;
		margin-block: 20px;
	}
	.customer-count {
		font-size: ${(props) => (props.size === "mobile" ? "18px" : "35px")};
		display: flex;
		justify-content: center;
		font-weight: bold;
		margin-inline: ${(props) => (props.size === "mobile" ? "5px" : "20px")};
		color :#800000
	}
	.indian-brand {
		font-size: ${(props) => (props.size === "mobile" ? "15px" : "35px")};
		display: flex;
		justify-content: center;
		margin-block: 20px;
		background-color:#800000;
		color: #fff;
		padding: ${(props) => (props.size === "mobile" ? "10px" : "0px")};
	}
`;
