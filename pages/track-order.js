import styled from "styled-components";
import Themes from "../themes/themes";
import React, { useState, useEffect } from "react";
import {
	Form,
	Input,
	Button,
	Collapse,
	Col,
	Row,
	message,
	Typography,
} from "antd";
import { useRouter } from "next/router";
import { useDispatch, useSelector } from "react-redux";

import Layout from "../layouts/HomeLayout";
import {
	UserOutlined,
	LockOutlined,
	MailOutlined,
	CaretRightOutlined,
} from "@ant-design/icons";
import BreadCrumb from "../components/BreadCrumb";
import AddressCard from "../components/AddressCard";
import ProfileAction from "../redux/actions/profile";
import axios from "axios";
import { API_URL } from "../constants";
import moment from "moment";
import Head from "next/head";
const { Panel } = Collapse;
const { Text } = Typography;
export default function Profile() {
	const [orders, setOrders] = useState([]);
	const [opened, setOpened] = useState([]);
	const router = useRouter();

	useEffect(() => {
		let token = localStorage.getItem("accessToken");
		if (!token) {
			message.warning("Please login before tracking order");
			router.push("/login");
		}
	}, []);
	const getOrders = () => {
		axios(API_URL + "/customer/orders", {
			headers: {
				Authorization: `JWT ${localStorage.getItem("accessToken")}`,
			},
		})
			.then((res) => {
				setOrders(res.data.data);
			})
			.catch((err) => {
				message.error(err.message);
			});
	};

	useEffect(() => {
		getOrders();
	}, []);

	useEffect(() => {
		console.log(orders);
	}, [orders]);

	return (
		<Wrapper>
			<Head>
				<title>TRICOTS </title>
			</Head>
			<Layout>
				<BreadCrumb
					data={[{ name: "Home", route: "/" }, { name: "Profile" }]}
				/>
				<div className="container">
					<Row style={{ margin: 10 }} justify="space-around">
						{orders.map((order) => {
							return (
								<Col
									xs={20}
									md={10}
									style={{
										boxShadow: "4px 6px 39px -6px #ddd",
										// width: "45%",
										padding: 20,
										marginTop: 20,
										// display: "flex",
										// flexDirection: "row",
										// justifyContent: "center",
										// alignItems: "center",
									}}
								>
									<p>
										<span>Order ID:</span>
										<span
											style={{
												marginLeft: 15,
												fontWeight: 600,
											}}
										>
											{order?.orderID}
										</span>
									</p>
									<p>
										<span>Tracking ID:</span>
										<span
											style={{
												marginLeft: 15,
												fontWeight: 600,
											}}
										>
											{order?.trackingId ? order?.trackingId : "-"}
										</span>
									</p>
									<p>
										<span>Tracking Link:</span>
										<span
											style={{
												marginLeft: 15,
												fontWeight: 600,
											}}
										>
											{order?.trackingLink ? (
												<a href={order?.trackingLink}> {order?.trackingLink} </a>
											) : (
												"-"
											)}
										</span>
									</p>
									<p>
										<span>Total Price:</span>
										<span
											style={{
												marginLeft: 15,
												fontWeight: 600,
											}}
										>
											{order?.totalPrice}
										</span>
									</p>

									<p>
										<span>Address:</span>
										<span
											style={{
												marginLeft: 15,
												fontWeight: 600,
											}}
										>
					<span className="address-name">
										<span className="special-address-text">{order?.address?.type}{`,  `}</span>
					</span>
					<span>{order?.address?.doorNumber + ",  "}</span>
					<span>{order?.address?.street + ",  "} </span>
					<span>{order?.address?.landmark + ",  "} </span>
					<span>{order?.address?.district + ",  "}</span>
					<span>{order?.address?.pincode + ",  "}</span>
					<div>{order?.address?.state + ",  INDIA"}</div>
										</span>
									</p>
									<p>
										<span>Status:</span>
										<span
											style={{
												marginLeft: 15,
												fontWeight: 600,
												color: Themes.black,
												// order?.status === "Ordered" ? "yellow" :Themes.green,
											}}
										>
											{order?.status}
										</span>
									</p>
									<p>
										<span>Payment Status:</span>
										<span
											style={{
												marginLeft: 15,
												fontWeight: 600,
												color: order?.isPaid ? Themes.green : Themes.red,
											}}
										>
											{order?.isPaid ? "PAID" : "PENDING"}
										</span>
									</p>
									<p>
										<span>Payment Method:</span>
										<span
											style={{
												marginLeft: 15,
												fontWeight: 600,
											}}
										>
											{order?.paymentMethod === "COD"
												? "Cash on Delivery"
												: "Online"}
										</span>
									</p>

									<p>
										<span>Ordered Date:</span>
										<span
											style={{
												marginLeft: 15,
												fontWeight: 600,
											}}
										>
											{moment(order?.createdAt).format("Do MMM YYYY ")}
										</span>
									</p>
									{order?.trackingId ? (
										""
									) : (
										<p>
											<span style={{ color: Themes.red }}>
												* Tracking ID and Tracking Link will be updated soon.
											</span>
										</p>
									)}
									{!opened.includes(order?._id) && (
										<span>
											<span
												onClick={() => {
													if (opened.includes(order?._id)) {
														setOpened(
															opened.filter((oo) => {
																if (oo !== order?._id) return oo;
															})
														);
													} else {
														setOpened([...opened, order?._id]);
													}
												}}
												style={{
													color: Themes.primary,
													cursor: "pointer",
												}}
											>
												Show more...
											</span>
										</span>
									)}

									{opened.includes(order?._id) && (
										<div>
											{order?.orderItems?.map((orderItem) => {
												return (
													<Row
														style={{
															display: "flex",
															flexDirection: "row",
															marginTop: 20,
															border: "1px solid #000",
															padding: 10,
														}}
														align="middle"
														gutter={[0, 24]}
													>
														<Col xs={8} md={4}>
															<img
																src={orderItem?.imageUrl}
																style={{
																	width: 50,
																	height: 50,
																}}
															/>
														</Col>
														<Col xs={16} md={4}>
															<span
																style={{
																	color: Themes.primary,
																	fontSize: 14,
																	fontWeight: 400,
																}}
															>
																{orderItem?.inventoryName}
															</span>
														</Col>
														<Col xs={6} md={4}>
															<div
																style={{
																	backgroundColor: orderItem?.color,
																	height: 33,
																	width: 33,
																	borderRadius: 50,
																	marginRight: 10,
																	cursor: "pointer",
																	border: "2px solid #000",
																}}
															/>
														</Col>
														<Col xs={5} md={4}>
															<Text
																style={{
																	fontWeight: "900",
																}}
															>
																{orderItem?.size}
															</Text>
														</Col>
														<Col xs={2} md={2}>
															<span
																style={{
																	color: Themes.backgroundMain,
																	fontSize: 14,
																	fontWeight: 400,
																}}
															>
																X
															</span>
														</Col>
														<Col xs={5} md={2}>
															<span
																style={{
																	color: Themes.backgroundMain,
																	fontSize: 14,
																	fontWeight: 400,
																}}
															>
																{orderItem?.quantity}
															</span>
														</Col>
														<Col xs={6} md={2}>
															<span
																style={{
																	color: Themes.backgroundMain,
																	fontSize: 14,
																	fontWeight: 400,
																}}
															>
																{" "}
																₹{" "}
																{orderItem?.specialPrice
																	? orderItem?.specialPrice !== 0
																		? orderItem?.specialPrice *
																		  orderItem?.quantity
																		: orderItem?.price * orderItem?.quantity
																	: null}
															</span>
														</Col>

														<span></span>
													</Row>
												);
											})}
										</div>
									)}
								</Col>
							);
						})}
					</Row>
				</div>
			</Layout>
		</Wrapper>
	);
}

const Wrapper = styled.section`
	font-family: "Montserrat", sans-serif;
	width: 100vw;
	.container {
		width: 100%;
		display: flex;
		justify-content: center;
		align-items: center;
		flex-direction: column;
		/* background-color: ${Themes.backgroundMain}; */
	}
	.register-form {
		/* max-width: 350px; */
	}
	.register-form-forgot {
		float: right;
	}
	.ant-col-rtl .register-form-forgot {
		float: left;
	}
	.register-form-button {
		width: 100%;
	}
	.register-form-button {
		width: 100%;
	}
	[data-theme="compact"]
		.site-collapse-custom-collapse
		.site-collapse-custom-panel,
	.site-collapse-custom-collapse1 .site-collapse-custom-panel {
		overflow: hidden;
		background: #ffffff;
		border: 0px;
		border-radius: 2px;
		width: 90vw;
	}
	.back-btn {
		border: 1px solid ${Themes.primary};
		color: ${Themes.primary};
		width: 100%;
	}
`;
