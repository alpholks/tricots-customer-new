import styled from "styled-components";
import Themes from "../themes/themes";
import React, { useState, useEffect } from "react";
import {
	Row,
	Col,
	Space,
	Dropdown,
	Carousel,
	Drawer,
	Button,
	Radio,
	Collapse,
	Grid,
	message,
} from "antd";
import Head from "next/head";
import Images from "../assests";
import {
	DownOutlined,
	SearchOutlined,
	LeftCircleFilled,
	RightCircleFilled,
	HeartFilled,
	ShoppingCartOutlined,
	MenuOutlined,
	CaretRightOutlined,
	PlusOutlined,
	MinusOutlined,
} from "@ant-design/icons";

import Header from "../components/Header";
import Footer from "../components/Footer";
import Categories from "../components/Categories";
import Collections from "../components/Collections";
import { BannerTop, BannerBottom } from "../components/Banner";
import HomeLayout from "../layouts/HomeLayout";
import { CartItem, ComboCartItem } from "../components/CartItem";
import BreadCrumb from "../components/BreadCrumb";
import { useRouter } from "next/router";
import { useDispatch, useSelector } from "react-redux";
import generalActions from "../redux/actions/general";
import cartActions from "../redux/actions/cart";
import comboCartartActions from "../redux/actions/comboCart";

const { Panel } = Collapse;
const { useBreakpoint } = Grid;

export default function Home() {
	// state = { visible: false, placement: 'left' };
	const screens = useBreakpoint();
	const dispatch = useDispatch();
	const router = useRouter();

	const {
		cart,
		addingToCart,
		addToCartSuccessful,

		removingFromCart,
		removeFromCartSuccessful,

		creatingCart,
		createCartSuccessful,

		deletingCart,
		deleteCartSuccessful,
	} = useSelector((state) => state.cart);

	const {
		comboCart,
		addingToComboCart,
		addToComboCartSuccessful,

		removingFromComboCart,
		removeFromComboCartSuccessful,

		creatingComboCart,
		createComboCartSuccessful,

		deletingComboCart,
		deleteComboCartSuccessful,
	} = useSelector((state) => state.comboCart);

	useEffect(() => {
		dispatch(
			cartActions.stateReset({
				addingToCart: false,
				addToCartSuccessful: false,

				removingFromCartDetails: false,
				removeFromCartSuccessful: false,

				creatingCart: false,
				createCartSuccessful: false,

				deletingCart: false,
				deleteCartSuccessful: false,
			})
		);
		dispatch(
			comboCartartActions.stateReset({
				addingToComboCart: false,
				addToComboCartSuccessful: false,

				removingFromComboCart: false,
				removeFromComboCartSuccessful: false,

				creatingComboCart: false,
				createComboCartSuccessful: false,

				deletingComboCart: false,
				deleteComboCartSuccessful: false,
			})
		);
	}, []);

	useEffect(() => {
		if (router.query.productId)
			dispatch(generalActions.getProductDetailsRequest(router.query.productId));
	}, [router.query.productId]);

	const handleClickPlus = (inventory, quantity) => {
		if (!cart || !cart[inventory._id]) {
			dispatch(
				cartActions.createCartRequest({
					[inventory._id]: {
						...inventory,
						productName: productDetails.name,
						specialPrice: productDetails.specialPrice,
						defaultPrice: productDetails.defaultPrice,
						quantity: quantity,
					},
				})
			);
		} else {
			dispatch(cartActions.addToCartRequest(inventory._id, quantity + 1));
		}
	};

	const handleClickMinus = (inventory, quantity) => {
		if (quantity === "delete") {
			return dispatch(cartActions.deleteCartRequest(inventory._id));
		}
		if (cart && cart[inventory._id] && cart[inventory._id].quantity === 1) {
			dispatch(cartActions.deleteCartRequest(inventory._id));
		} else {
			dispatch(cartActions.addToCartRequest(inventory._id, quantity - 1));
		}
	};
	const deleteComboCart = (inventory, quantity) => {
		if (quantity === "delete") {
			return dispatch(
				comboCartartActions.deleteComboCartRequest(inventory.comboId)
			);
		}
	};

	useEffect(() => {
		if (
			createCartSuccessful ||
			addToCartSuccessful ||
			removeFromCartSuccessful ||
			deleteCartSuccessful ||
			addingToComboCart ||
			addToComboCartSuccessful ||
			removingFromComboCart ||
			removeFromComboCartSuccessful ||
			creatingComboCart ||
			createComboCartSuccessful ||
			deletingComboCart ||
			deleteComboCartSuccessful
		) {
			dispatch(
				cartActions.stateReset({
					addingToCart: false,
					addToCartSuccessful: false,

					removingFromCartDetails: false,
					removeFromCartSuccessful: false,

					creatingCart: false,
					createCartSuccessful: false,

					deletingCart: false,
					deleteCartSuccessful: false,
				})
			);
			dispatch(
				comboCartartActions.stateReset({
					addingToComboCart: false,
					addToComboCartSuccessful: false,

					removingFromComboCart: false,
					removeFromComboCartSuccessful: false,

					creatingComboCart: false,
					createComboCartSuccessful: false,

					deletingComboCart: false,
					deleteComboCartSuccessful: false,
				})
			);
		}
	}, [
		createCartSuccessful,
		addToCartSuccessful,
		removeFromCartSuccessful,
		deleteCartSuccessful,
		addToComboCartSuccessful,
		createComboCartSuccessful,
		removeFromComboCartSuccessful,
		deleteComboCartSuccessful,
	]);

	useEffect(() => {
		dispatch(cartActions.clearError());
	}, []);

	useEffect(() => {
		if (!creatingCart && createCartSuccessful) {
			cartActions.stateReset({
				createCartSuccessful: false,
			});
			message.success("Item added to cart successfully");
		}
	}, [createCartSuccessful]);

	useEffect(() => {
		if (!deletingCart && deleteCartSuccessful) {
			cartActions.stateReset({
				deleteCartSuccessful: false,
			});
			message.success("Cart updated successfully");
		}
	}, [deleteCartSuccessful]);

	useEffect(() => {
		if (!addingToCart && addToCartSuccessful) {
			cartActions.stateReset({
				addToCartSuccessful: false,
			});
			message.success("Item in cart updated successfully");
		}
	}, [addToCartSuccessful]);

	useEffect(() => {
		if (!removingFromCart && removeFromCartSuccessful) {
			cartActions.stateReset({
				removeFromCartSuccessful: false,
			});
			message.success("Item in a cart removed successfully");
		}
	}, [removeFromCartSuccessful]);

	return (
		<Wrapper view={screens.xs ? "mobile" : "web"}>
			<Head>
				<title>TRICOTS </title>
			</Head>
			<HomeLayout>
				<BreadCrumb data={[{ name: "Home", route: "/" }, { name: "Cart" }]} />
				<div className="container">
					<Button className="back-btn" onClick={() => router.back()}>
						Continue Shopping
					</Button>
					<Col
						xs={24}
						lg={12}
						style={{
							display: "flex",
							justifyContent: "center",
							flexDirection: "column",
						}}
					>
						{Object.keys(cart).map((item) => {
							return (
								<CartItem
									data={cart[item]}
									cart={{
										cart,
										createCartSuccessful,
										addToCartSuccessful,
										removeFromCartSuccessful,
										deleteCartSuccessful,
									}}
									comboCart={{
										comboCart,
										addToComboCartSuccessful,
										createComboCartSuccessful,
										removeFromComboCartSuccessful,
										deleteComboCartSuccessful,
									}}
									onClickPlus={(inventory, quantity) =>
										handleClickPlus(inventory, quantity)
									}
									onClickMinus={(inventory, quantity) =>
										handleClickMinus(inventory, quantity)
									}
									onClickCross={(inventory) =>
										handleClickMinus(inventory, "delete")
									}
								/>
							);
						})}
						{Object.keys(comboCart).map((item) => {
							return (
								<ComboCartItem
									data={comboCart[item]}
									comboCart={{
										comboCart,
										addToComboCartSuccessful,
										createComboCartSuccessful,
										removeFromComboCartSuccessful,
										deleteComboCartSuccessful,
									}}
									onClickCross={(inventory) =>
										deleteComboCart(inventory, "delete")
									}
								/>
							);
						})}

						{
						// !screens.xs &&
						(Object.keys(cart).length !== 0 ||
							Object.keys(comboCart).length !== 0) ? (
							<Button
								className="checkout-btn-web"
								type="primary"
								onClick={() => router.push("/checkout")}
							>
								Proceed & Checkout
							</Button>
						)
						 : null
						}

						{/* {screens.xs &&
						(Object.keys(cart).length !== 0 ||
							Object.keys(comboCart).length !== 0) ? (
							<Button
								className="checkout-btn"
								type="primary"
								onClick={() => router.push("/checkout")}
							>
								Proceed & Checkout
							</Button>
						) : null} */}
					</Col>
				</div>
			</HomeLayout>
		</Wrapper>
	);
}

const Wrapper = styled.section`
	font-family: "Montserrat", sans-serif;
	width: 100vw;
	width: 100%;
	.container {
		display: flex;
		flex-direction: column;
		justify-content: center;
		align-items: center;
		padding: 5px;
	}
	.checkout-btn {
		height: 40px;
		display: flex;
		justify-content: center;
		align-items: center;
		/* background-color: ${Theme.primary}; */
		width: 100vw;
		/* color: ${Theme.white}; */
		position: fixed;
		bottom: 0;
		text-transform: uppercase;
		z-index: 100;
		margin-left: -7px;
	}
	.checkout-btn-web {
		margin-top: 20px;
		z-index: 100;
		margin-bottom: 20px;
		width: ${(props) => (props.view == "mobile" ? "100%" : "200px")};
		height: 40px;
	}
	.back-btn {
		border: 1px solid ${Themes.primary};
		color: ${Themes.primary};
		margin-bottom: 10px;
		width: ${(props) => (props.view == "mobile" ? "100%" : "200px")};
		height: 40px;
	}
`;
