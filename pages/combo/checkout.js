import styled from "styled-components";
import Themes from "../../themes/themes";
import React, { useState, useEffect, useDebugValue } from "react";
import {
	Row,
	Col,
	Space,
	Dropdown,
	Carousel,
	Drawer,
	Button,
	Radio,
	Collapse,
	Grid,
	message,
} from "antd";
import Theme from "../../themes/themes";
import Head from "next/head";
import Images from "../../assests";
import {
	DownOutlined,
	SearchOutlined,
	LeftCircleFilled,
	RightCircleFilled,
	HeartFilled,
	ShoppingCartOutlined,
	MenuOutlined,
	CaretRightOutlined,
	PlusOutlined,
	MinusOutlined,
} from "@ant-design/icons";

import Header from "../../components/Header";
import Footer from "../../components/Footer";
import Categories from "../../components/Categories";
import Collections from "../../components/Collections";
import { BannerTop, BannerBottom } from "../../components/Banner";
import HomeLayout from "../../layouts/HomeLayout";
import CartItem from "../../components/CartItem";
import AddressCard from "../../components/AddressCard";
import BreadCrumb from "../../components/BreadCrumb";
import { useRouter } from "next/router";
import { useDispatch, useSelector } from "react-redux";
import generalActions from "../../redux/actions/general";
import cartActions from "../../redux/actions/cart";
import ProfileAction from "../../redux/actions/profile";
import Razorpay from "razorpay";

import axios from "axios";
import { API_URL } from "../../constants";

const { Panel } = Collapse;
const { useBreakpoint } = Grid;
export default function Home() {
	// state = { visible: false, placement: 'left' };

	function loadScript(src) {
		return new Promise((resolve) => {
			const script = document.createElement("script");
			script.src = src;
			script.onload = () => {
				resolve(true);
			};
			script.onerror = () => {
				resolve(false);
			};
			document.body.appendChild(script);
		});
	}

	const screens = useBreakpoint();
	const dispatch = useDispatch();
	const router = useRouter();
	const { error, addresses, profilDetails } = useSelector(
		(state) => state.profile
	);
	const [total, setTotal] = useState(0);
	const [discount, setDiscount] = useState(0);
	const [totalPayable, setTotalPayable] = useState(0);
	const [cartTotal, setCartTotal] = useState(0);
	const [shipping, setShipping] = useState(0);
	const [selectedAddress, setSelectedAddress] = useState();
	const [radio, setRadio] = useState();
	const [loading, setLoading] = useState(false);
	const [comboId, setComboId] = useState("");

	useEffect(() => {
		let token = localStorage.getItem("accessToken");
		if (!token) {
			message.warn("Please login before placing order");
			return router.push("/login");
		}
	}, []);

	useEffect(() => {
		dispatch(
			ProfileAction.stateReset({
				updatingProfileDetails: false,
				updateProfileDetailsSuccessful: false,
				fetchingProfileDetails: false,
				fetchProfileDetailsSuccessful: false,
				fetchingAddresses: false,
				fetchAddressesSuccessful: false,
			})
		);
	}, []);

	useEffect(() => {
		if (error) {
			message.error(error);
		}
	}, [error]);

	const {
		cart,
		addingToCart,
		addToCartSuccessful,

		removingFromCart,
		removeFromCartSuccessful,

		creatingCart,
		createCartSuccessful,

		deletingCart,
		deleteCartSuccessful,
	} = useSelector((state) => state.cart);

	useEffect(() => {
		dispatch(
			cartActions.stateReset({
				addingToCart: false,
				addToCartSuccessful: false,

				removingFromCartDetails: false,
				removeFromCartSuccessful: false,

				creatingCart: false,
				createCartSuccessful: false,

				deletingCart: false,
				deleteCartSuccessful: false,
			})
		);
	}, []);

	useEffect(() => {
		dispatch(ProfileAction.getAddressesRequest());
		dispatch(ProfileAction.getProfileDetailsRequest());
	}, []);

	useEffect(() => {
		if (router.query.productId)
			dispatch(
				generalActions.getProductDetailsRequest(router.query.productId)
			);
	}, [router.query.productId]);

	const handleClickPlus = (inventory) => {
		if (!cart || !cart[inventory._id]) {
			dispatch(
				cartActions.createCartRequest({
					[inventory._id]: { ...inventory, quantity: 1 },
				})
			);
		} else {
			dispatch(cartActions.addToCartRequest(inventory._id));
		}
	};

	const handleClickMinus = (inventory, method = "remove") => {
		if (method === "delete") {
			dispatch(cartActions.deleteCartRequest(inventory._id));
		}
		if (cart && cart[inventory._id] && cart[inventory._id].quantity === 1) {
			dispatch(cartActions.deleteCartRequest(inventory._id));
		} else {
			dispatch(cartActions.removeFromCartRequest(inventory._id));
		}
	};

	useEffect(() => {
		if (!creatingCart && createCartSuccessful) {
			cartActions.stateReset({
				createCartSuccessful: false,
			});
		}
	}, [createCartSuccessful]);

	useEffect(() => {
		if (!deletingCart && deleteCartSuccessful) {
			cartActions.stateReset({
				deleteCartSuccessful: false,
			});
		}
	}, [deleteCartSuccessful]);

	useEffect(() => {
		if (!addingToCart && addToCartSuccessful) {
			cartActions.stateReset({
				addToCartSuccessful: false,
			});
		}
	}, [addToCartSuccessful]);

	useEffect(() => {
		if (!removingFromCart && removeFromCartSuccessful) {
			cartActions.stateReset({
				removeFromCartSuccessful: false,
			});
		}
	}, [removeFromCartSuccessful]);

	useEffect(() => {
		var total = 0;
		var discount = 0;
		var cartTotal = 0;
		var shipping = 0;
		var totalPayable = 0;
		var comboId = "";

		Object.keys(cart).map((item) => {
			total = cart[item].defaultPrice * cart[item].quantity;
			comboId = cart[item].comboId 
			discount =
				(cart[item].defaultPrice -
					(cart[item].specialPrice ? cart[item].specialPrice : 0)) *
				cart[item].quantity;
			cartTotal =
				(cart[item].specialPrice
					? cart[item].specialPrice
					: cart[item].defaultPrice) * cart[item].quantity;
		});
		totalPayable = cartTotal + shipping;

		setTotal(total);
		setDiscount(discount);
		setCartTotal(cartTotal);
		setShipping(shipping);
		setTotalPayable(totalPayable);
		setComboId(comboId);
	}, [cart]);

	const handlePlaceOrder = async () => {
		setLoading(true);
		if (!selectedAddress) {
			setLoading(false);
			return message.warn("Select address to proceed");
		}
		if (!radio) {
			setLoading(false);
			return message.warn("Select payment mode to proceed");
		}
		let inventories = [];
		await Promise.all(
			Object.keys(cart)?.map((cartItem) => {
				inventories.push({
					inventoryId: cartItem,
					quantity: cart[cartItem].quantity,
				});
			})
		);
		const data = {
			inventories: inventories,
			paymentMethod: radio,
			address: selectedAddress,
			deliveryCharge: shipping,
			isCombo: true,
			comboId : comboId,
			totalPrice: cartTotal,
		};

		axios
			.post(`${API_URL}/customer/orders`, data, {
				headers: {
					authorization: `JWT ${await localStorage.getItem(
						"accessToken"
					)}`,
				},
			})
			.then((result) => {
				if (result.data.apiStatus === "SUCCESS") {
					localStorage.setItem("cart", JSON.stringify({}));
					message.success("order placed successfully");
				} else {
					message.error("FAILED to place order");
				}
				setLoading(false);
			})
			.catch((error) => {
				message.error(error.message);
				setLoading(false);
			});
	};

	async function displayRazorpay() {
		setLoading(true);

		if (!selectedAddress) {
			setLoading(false);
			return message.warn("Select address to proceed");
		}
		if (!radio) {
			setLoading(false);
			return message.warn("Select payment mode to proceed");
		}
		const res = await loadScript(
			"https://checkout.razorpay.com/v1/checkout.js"
		);

		if (!res) {
			alert("Razorpay SDK failed to load. Are you online?");
			return;
		}

		let totalAmount = totalPayable;
		// if (promocodeVerified) {
		// 	totalAmount = totalAmount - (total * promocode.discountPercentage) / 100;
		// }
		// if (radio === "cashOnDelivery") {
		// 	totalAmount += totalAmount + 40;
		// }

		axios
			.post(
				`${API_URL}/customer/orders/create`,
				{
					amount: totalAmount,
					currency: "INR",
				},
				{
					headers: {
						authorization: `JWT ${await localStorage.getItem(
							"accessToken"
						)}`,
					},
				}
			)
			.then((result) => {
				setLoading(false);

				if (!result) {
					alert("Server error. Are you online?");
					return;
				}

				const { amount, id: order_id, currency } = result.data.data;

				const options = {
					key: "rzp_test_O3p5Lzusyeoodk", // Enter the Key ID generated from the Dashboard
					amount: amount.toString(),
					currency: currency,
					name: "Tricots.in",
					description: "Appearel",
					image:
						"https://lukantake-web.vercel.app/_next/static/images/logo-9ed6854087858478eafb175b4cdbf0ca.jpg",
					order_id: order_id,
					handler: async function (response) {
						let inventories = [];
						await Promise.all(
							Object.keys(cart)?.map((cartItem) => {
								inventories.push({
									inventoryId: cartItem,
									quantity: cart[cartItem].quantity,
								});
							})
						);
						const data = {
							inventories: inventories,
							paymentMethod: "ONLINE",
							address: selectedAddress,
							deliveryCharge: shipping,
							totalPrice: cartTotal,
							isCombo: true,
							comboId : comboId,
							paymentDetails: {
								orderCreationId: order_id,
								razorpayPaymentId: response.razorpay_payment_id,
								razorpayOrderId: response.razorpay_order_id,
								razorpaySignature: response.razorpay_signature,
							},
						};

						setLoading(true);

						axios
							.post(`${API_URL}/customer/orders`, data, {
								headers: {
									authorization: `JWT ${await localStorage.getItem(
										"accessToken"
									)}`,
								},
							})
							.then((result) => {
								if (result.data.apiStatus === "SUCCESS") {
									setLoading(false);

									localStorage.setItem(
										"cart",
										JSON.stringify({})
									);
									// return router.push("track-order");
								} else {
									// result.error.apiStatus === "FAILURE";
									setLoading(false);
									message.error("FAILED to place order");
								}
							})
							.catch((error) => {
								message.error(error.message);
								setLoading(false);
							});
					},
					prefill: {
						name:
							profilDetails?.firstName +
							" " +
							profilDetails?.lastName,
						email: profilDetails?.emailId,
						contact: profilDetails?.phoneNumber,
					},
					notes: {
						address:
							selectedAddress?.doorNumber +
							", " +
							selectedAddress?.street +
							", " +
							selectedAddress?.landmark +
							", ",
					},
					theme: {
						color: Theme.primary,
					},
				};

				const paymentObject = new window.Razorpay(options);
				paymentObject.open();
			})
			.catch((error) => {
				message.error(error.message);
				setLoading(false);
			});
	}

	return (
		<Wrapper>
			<HomeLayout>
				<BreadCrumb
					data={[{ name: "Home", route: "/" }, { name: "Checkout" }]}
				/>
				<div className="container">
					<Row gutter={[0, 24]}>
						<Col xs={24} lg={8}>
							<div style={{ padding: screens.xs ? 0 : 20 }}>
								<p className="checkout-headings">Your Cart</p>
								<Button
									className="back-btn"
									onClick={() => router.back()}
								>
									Continue Shopping
								</Button>

								{Object.keys(cart).map((item) => {
									return (
										<CartItem
											comboPage={true}
											data={cart[item]}
											cart={{
												cart,
												createCartSuccessful,
												addToCartSuccessful,
												removeFromCartSuccessful,
												deleteCartSuccessful,
											}}
											onClickPlus={(inventory) =>
												handleClickPlus(inventory)
											}
											onClickMinus={(inventory) =>
												handleClickMinus(inventory)
											}
											onClickCross={(inventory) =>
												handleClickMinus(
													inventory,
													"delete"
												)
											}
										/>
									);
								})}
							</div>
						</Col>
						<Col xs={24} lg={8}>
							<div style={{ padding: screens.xs ? 0 : 20 }}>
								<p className="checkout-headings">
									Select Address
								</p>
								<Button className="back-btn">
									Create New Address
								</Button>
								{addresses?.map((address) => {
									return (
										<AddressCard
											onSelectAddress={(address) =>
												setSelectedAddress(address)
											}
											checked={
												selectedAddress?._id ===
												address._id
											}
											showRadio
											data={address}
											showActions={true}
											onClickEdit={() =>
												router.push(
													"/address/" + address._id
												)
											}
											onClickDelete={() => {}}
										/>
									);
								})}
							</div>
						</Col>

						<Col xs={24} lg={8}>
							<div style={{ padding: screens.xs ? 0 : 20 }}>
								<div style={{ marginBlock: "15px" }}>
									<p className="checkout-headings">
										Payment Mode
									</p>

									<Radio.Group
										onChange={(e) =>
											setRadio(e.target.value)
										}
									>
										<Radio value={"ONLINE"}>Online</Radio>
										<Radio value={"COD"}>
											Cash On Delivery
										</Radio>
									</Radio.Group>
								</div>
							</div>
							<div style={{ padding: screens.xs ? 0 : 20 }}>
								<div style={{ marginBlock: "15px" }}>
									<p className="checkout-headings">
										Payments
									</p>

									<Row justify="space-between">
										<Col>
											Total MRP(Inclusive Of all Taxes)
										</Col>
										<Col className="price-text">
											₹{total}
										</Col>
									</Row>
									<Row justify="space-between">
										<Col>Tricots Discount </Col>
										<Col className="price-text">
											-₹{discount}
										</Col>
									</Row>
									<Row justify="space-between">
										<Col>Cart Total </Col>
										<Col className="price-text">
											₹{cartTotal}
										</Col>
									</Row>
									<Row justify="space-between">
										<Col>Shipping</Col>
										<Col className="free-text">
											{shipping === 0
												? "FREE"
												: `₹${shipping}`}
										</Col>
									</Row>
									<Row justify="space-between">
										<Col>Total Payable </Col>
										<Col className="price-text">
											₹{totalPayable}
										</Col>
									</Row>
									<Row justify="center">
										<Col className="free-text">
											Congrats! You Saved ₹{discount} On
											This Order
										</Col>
									</Row>
									<Row justify="center">
										{!screens.xs && (
											<Button
												loading={loading}
												onClick={() =>
													{	if(radio === undefined){
														return message.error("Select Payment Mode to Proceed")
													}else {
														return radio === "ONLINE"
														? displayRazorpay()
														: radio === "COD"
														? handlePlaceOrder()
														: {}
													}}
												}
												className="checkout-btn-web"
												type="primary"
											>
												Continue To Payment
											</Button>
										)}
									</Row>
								</div>
							</div>
						</Col>
					</Row>
				</div>
				{screens.xs && (
					<Button
						loading={loading}
						className="checkout-btn"
						type="primary"
						onClick={() =>
							{	if(radio === undefined){
                                return message.error("Select Payment Mode to Proceed")
                            }else {
                                return radio === "ONLINE"
                                ? displayRazorpay()
                                : radio === "COD"
                                ? handlePlaceOrder()
                                : {}
                            }}
						}
					>
						Continue To Payment
					</Button>
				)}
			</HomeLayout>
		</Wrapper>
	);
}

const Wrapper = styled.section`
	font-family: "Montserrat", sans-serif;
	width: 100vw;
	width: 100%;
	.container {
		display: flex;
		flex-direction: column;
		padding: 5px;
	}
	.checkout-btn {
		width: 100%;
		margin-top: 20px;
		position: fixed;
		bottom: 0;
		z-index: 100;
		height: 40px;
	}
	.checkout-btn-web {
		width: 100%;
		margin-top: 20px;
		z-index: 100;
		height: 40px;
	}
	.back-btn {
		border: 1px solid ${Themes.primary};
		color: ${Themes.primary};
		margin-bottom: 10px;
		width: 100%;
		height: 40px;
	}
	.checkout-headings {
		font-size: 16px;
		font-weight: 500;
		margin-top: 10px;
	}
	.price-text {
		font-size: 16px;
		font-weight: 500;
	}
	.free-text {
		/* font-size:16px; */
		font-weight: 500;
		color: ${Themes.positive};
	}
`;
