import styled from "styled-components";
import React, { useEffect, useState } from "react";
import Head from "next/head";
import { Row, Col, message, Grid } from "antd";

import Theme from "../../themes/themes";
import Layout from "../../layouts/HomeLayout";
import BreadCrumb from "../../components/BreadCrumb/BreadCrumb";
import { BannerTop, BannerBottom } from "../../components/Banner/Banner";
import ProductCard from "../../components/ProductCard/ProductCard";
import ProductCarousal from "../../components/ProductCarousal/ProductCarousal";
import ComboPack from "../../components/ComboPack/ComboPack";
import { ShoppingCartOutlined, EyeOutlined } from "@ant-design/icons";
import { useRouter } from "next/router";
import { useDispatch, useSelector } from "react-redux";
import generalActions from "../../redux/actions/general";
import cartActions from "../../redux/actions/comboCart";

const { useBreakpoint } = Grid;

export default function ProductView() {
	const screens = useBreakpoint();
	const dispatch = useDispatch();
	const router = useRouter();
	const [cartData, setCartData] = useState({});
	const { comboDetails, products } = useSelector((state) => state.general);
	const {
		comboCart,
		addingToComboCart,
		addToComboCartSuccessful,

		removingFromComboCart,
		removeFromComboCartSuccessful,

		creatingComboCart,
		createComboCartSuccessful,

		deletingComboCart,
		deleteComboCartSuccessful,
	} = useSelector((state) => state.comboCart);

	useEffect(() => {
		dispatch(
			cartActions.stateReset({
				addingToComboCart: false,
				addToComboCartSuccessful: false,

				removingFromComboCartDetails: false,
				removeFromComboCartSuccessful: false,

				creatingComboCart: false,
				createComboCartSuccessful: false,

				deletingComboCart: false,
				deleteComboCartSuccessful: false,
			})
		);
	}, []);

	useEffect(() => {
		if (router.query.comboId)
			dispatch(generalActions.getComboDetailsRequest(router.query.comboId));
	}, [router.query.comboId]);

	const handleClickProduct = (product) => {
		console.log(router.asPath.split("/").splice(1)[0] + "/" + product.slug);
		if (router.asPath.split("/").splice(1)[0])
			return router.push(
				"/" + router.asPath.split("/").splice(1)[0] + "/" + product.slug
			);
		router.push(product.slug);
	};

	const handleClickPlus = (inventory) => {
		if (cartData[comboDetails._id] !== undefined) {
			setCartData({
				...cartData,
				[comboDetails._id]: {
					...cartData[comboDetails._id],
					[inventory._id]: inventory,
				},
			});

			// let det = comboCart;
			// let inventories = det[comboDetails._id][inventory._id]
			// det.inventories = inventories
			// setCartData(det);
		} else {
			setCartData({
				...cartData,
				[comboDetails._id]: {
					productName: comboDetails.name,
					specialPrice: comboDetails.specialPrice,
					defaultPrice: comboDetails.defaultPrice,
					imageUrl: comboDetails.imageId?.imageUrl,
					comboId: comboDetails?._id,
					[inventory._id]: inventory,
				},
			});
		}

		// dispatch(
		// 	cartActions.createComboCartRequest({
		// 		[inventory._id]: {
		// 			...inventory,
		// 			productName: comboDetails.name,
		// 			specialPrice: comboDetails.specialPrice,
		// 			defaultPrice: comboDetails.defaultPrice,
		// 			quantity: 1,
		// 			comboId: comboDetails._id,
		// 		},
		// 	})
		// );
	};

	const handleClickMinus = (inventory) => {
		if (
			comboCart &&
			comboCart[inventory._id] &&
			comboCart[inventory._id].quantity === 1
		) {
			dispatch(cartActions.deleteComboCartRequest(inventory._id));
		} else {
			dispatch(cartActions.removeFromComboCartRequest(inventory._id));
		}
	};

	const handleClickAddToCart = () => {
		if (
			cartData !== undefined &&
			cartData[comboDetails._id] &&
			Object.keys(cartData ? cartData[comboDetails._id] : {})?.length - 5 ===
				comboDetails.numberOfItems
		) {
			dispatch(cartActions.createComboCartRequest(cartData));
		} else {
			message.warn(`Select ${comboDetails.numberOfItems} items to proceed`);
		}
	};

	useEffect(() => {
		if (
			createComboCartSuccessful ||
			addToComboCartSuccessful ||
			removeFromComboCartSuccessful ||
			deleteComboCartSuccessful
		) {
			dispatch(
				cartActions.stateReset({
					addToComboCartSuccessful: false,
					removeFromComboCartSuccessful: false,
					createComboCartSuccessful: false,
					deleteComboCartSuccessful: false,
				})
			);
			router.push("/cart");
		}
	}, [
		createComboCartSuccessful,
		addToComboCartSuccessful,
		removeFromComboCartSuccessful,
		deleteComboCartSuccessful,
	]);

	useEffect(() => {
		if (!creatingComboCart && createComboCartSuccessful) {
			cartActions.stateReset({
				createComboCartSuccessful: false,
			});
		}
	}, [createComboCartSuccessful]);

	useEffect(() => {
		if (!deletingComboCart && deleteComboCartSuccessful) {
			cartActions.stateReset({
				deleteComboCartSuccessful: false,
			});
		}
	}, [deleteComboCartSuccessful]);

	useEffect(() => {
		if (!addingToComboCart && addToComboCartSuccessful) {
			cartActions.stateReset({
				addToComboCartSuccessful: false,
			});
		}
	}, [addToComboCartSuccessful]);

	useEffect(() => {
		if (!removingFromComboCart && removeFromComboCartSuccessful) {
			cartActions.stateReset({
				removeFromComboCartSuccessful: false,
			});
		}
	}, [removeFromComboCartSuccessful]);

	return (
		<Wrapper>
			<Head>
				<title>TRICOTS </title>
			</Head>
			<Layout>
				{/* <BannerTop /> */}
				<BreadCrumb
					data={[{ name: "Home", route: "/" }, { name: "Combo Full Sleeve" }]}
				/>
				{/* <BannerBottom /> */}

				<div className="container">
					<Row gutter={[0, 12]}>
						<Col xs={24} sm={24} md={24} l={24} xl={24}>
							<ComboPack
								cart={{
									comboCart,
									createComboCartSuccessful,
									addToComboCartSuccessful,
									removeFromComboCartSuccessful,
									deleteComboCartSuccessful,
								}}
								data={comboDetails}
								onClickPlus={(inventory) => handleClickPlus(inventory)}
								onClickMinus={(inventory) => handleClickMinus(inventory)}
								onClickAddToCart={handleClickAddToCart}
								onClickViewCart={() => router.push("/cart")}
							/>
						</Col>
					</Row>
					<Row gutter={[0, 12]}>
						{products.map((product) => {
							return (
								<Col xs={12} sm={12} md={12} l={8} xl={6}>
									<ProductCard
										data={product}
										onClick={() => handleClickProduct(product)}
									/>
								</Col>
							);
						})}
					</Row>
				</div>
				{/* {screens.xs && Object.keys(comboCart).length !== 0 ? (
					<Button
						type={"primary"}
						icon={<EyeOutlined />}
						className="add-to-cart-btn"
						onClick={() => router.push("/cart")}
					>
						View Cart ({Object.keys(comboCart).length})
					</Button>
				) : null} */}
				{/* <div className="add-to-cart-btn"><EyeOutlined  className="icon" />View Cart (12)</div> */}
			</Layout>
		</Wrapper>
	);
}

const Wrapper = styled.section`
	font-family: "Montserrat", sans-serif;
	width: 100vw;
	width: 100%;
	.container {
		padding: 7px;
	}
	.add-to-cart-btn {
		height: 40px;
		display: flex;
		justify-content: center;
		align-items: center;
		background-color: ${Theme.primary};
		width: 100vw;
		color: ${Theme.white};
		position: fixed;
		bottom: 0;
		text-transform: uppercase;
		z-index: 100;
		margin-left: -7px;
	}
	.icon {
		margin-right: 15px;
		font-size: 22px;
		text-transform: uppercase;
	}
`;
