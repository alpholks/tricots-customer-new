import styled from "styled-components";
import React, { useEffect } from "react";
import Head from "next/head";
import { Row, Col } from "antd";

import Themes from "../../themes/themes";
import Layout from "../../layouts/HomeLayout";
import BreadCrumb from "../../components/BreadCrumb";
import { BannerTop, BannerBottom } from "../../components/Banner";
import ProductCard from "../../components/ProductCard";
import { useRouter } from "next/router";
import { useDispatch, useSelector } from "react-redux";
import generalActions from "../../redux/actions/general";

export default function Signup() {
	const dispatch = useDispatch();
	const router = useRouter();
	const { combo, fetchComboSuccessful, fetchingCombo } = useSelector(
		(state) => state.general
	);

	useEffect(() => {
		dispatch(generalActions.getComboRequest());
	}, []);

	useEffect(() => {
		if (!fetchingCombo && fetchComboSuccessful)
			dispatch(generalActions.getComboRequest());
	}, [fetchComboSuccessful]);

	const handleClickProduct = (product) => {
		router.push("/combo/" + product.slug);
	};
	return (
		<Wrapper>
			<Head>
				<title>TRICOTS </title>
			</Head>
			<Layout>
				<BannerTop data={[]} />
				<BreadCrumb
					data={[
						{ name: "Home", route: "/" },
						{ name: "Men's Tshirt" },
					]}
				/>
				{/* <BannerBottom /> */}
				<div className="container">
					<Row gutter={[0, 12]}>
						{combo?.map((product) => {
							return (
								<Col xs={12} sm={12} md={12} l={8} xl={6}>
									<ProductCard
										hideFavorite={true}
										data={product}
										onClick={() =>
											handleClickProduct(product)
										}
									/>
								</Col>
							);
						})}
					</Row>
				</div>
			</Layout>
		</Wrapper>
	);
}

const Wrapper = styled.section`
	font-family: "Montserrat", sans-serif;
	width: 100vw;
	width: 100%;
	.container {
		padding: 7px;
	}
`;
