import styled from "styled-components";
import Themes from "../themes/themes";
import React, { useState, useEffect } from "react";
import { Form, Input, Button, Collapse, Col, Row, message } from "antd";
import { useRouter } from "next/router";
import { useDispatch, useSelector } from "react-redux";
import Head from "next/head";
import Layout from "../layouts/HomeLayout";
import {
	UserOutlined,
	LockOutlined,
	MailOutlined,
	CaretRightOutlined,
} from "@ant-design/icons";
import BreadCrumb from "../components/BreadCrumb";
import AddressCard from "../components/AddressCard";
import ProfileAction from "../redux/actions/profile";
import { GoogleLogout } from "react-google-login";

const { Panel } = Collapse;

export default function Profile() {
	const [form] = Form.useForm();

	const router = useRouter();
	const dispatch = useDispatch();

	const {
		error,
		updatingProfileDetails,
		updateProfileDetailsSuccessful,
		fetchingProfileDetails,
		fetchProfileDetailsSuccessful,
		profileDetails,
		fetchingAddresses,
		fetchAddressesSuccessful,
		addresses,
	} = useSelector((state) => state.profile);

	useEffect(() => {
		dispatch(ProfileAction.clearError());
	}, []);

	const onFinish = () => {};

	useEffect(() => {
		dispatch(
			ProfileAction.stateReset({
				updatingProfileDetails: false,
				updateProfileDetailsSuccessful: false,
				fetchingProfileDetails: false,
				fetchProfileDetailsSuccessful: false,
				fetchingAddresses: false,
				fetchAddressesSuccessful: false,
			})
		);
	}, []);

	useEffect(() => {
		if (error) {
			message.error(error);
		}
	}, [error]);

	useEffect(() => {
		dispatch(ProfileAction.getProfileDetailsRequest());
		dispatch(ProfileAction.getAddressesRequest());
	}, []);

	useEffect(() => {
		if (!fetchingProfileDetails && fetchProfileDetailsSuccessful) {
			dispatch(
				ProfileAction.stateReset({
					fetchProfileDetailsSuccessful: false,
				})
			);
			form.setFieldsValue({
				firstName: profileDetails.firstName,
				lastName: profileDetails.lastName,
				emailId: profileDetails.emailId,
			});
		}
	}, [fetchProfileDetailsSuccessful]);

	useEffect(() => {
		if (!updatingProfileDetails && updateProfileDetailsSuccessful) {
			dispatch(
				ProfileAction.stateReset({
					updateProfileDetailsSuccessful: false,
				})
			);
			message.success("Profile updated successfully");
		}
		dispatch(ProfileAction.getProfileDetailsRequest());
	}, [updateProfileDetailsSuccessful]);

	const [editMode, setEditMode] = useState("");

	const handleChangeEditMode = (panel) => {
		setEditMode(panel);
	};
	const hanldeSaveProfile = ({ firstName, lastName }) => {
		dispatch(
			ProfileAction.updateProfileDetailsRequest({
				firstName: firstName,
				lastName: lastName,
			})
		);
	};
	return (
		<Wrapper>
			<Head>
				<title>TRICOTS </title>
			</Head>
			<Layout>
				<BreadCrumb
					data={[{ name: "Home", route: "/" }, { name: "Profile" }]}
				/>

				<div className="container">
					<Collapse
						bordered={false}
						defaultActiveKey={[1, 2, 3]}
						expandIcon={({ isActive }) => (
							<CaretRightOutlined rotate={isActive ? 90 : 0} />
						)}
						className="site-collapse-custom-collapse1"
					>
						<Panel
							header="Profile"
							key={1}
							className="site-collapse-custom-panel"
						>
							<Form
								form={form}
								name="normal_register"
								className="register-form"
								initialValues={{
									remember: true,
								}}
								onFinish={hanldeSaveProfile}
							>
								<Row gutter={48}>
									<Col xs={24} lg={12}>
										<Form.Item
											name="firstName"
											rules={[
												{
													required: true,
													message: "Please input your First Name!",
												},
											]}
										>
											<Input
												onChange={(e) =>
													form.setFieldsValue({
														firstName: e.target.value,
													})
												}
												disabled={editMode !== "profile"}
												prefix={
													<UserOutlined className="site-form-item-icon" />
												}
												placeholder="First Name"
											/>
										</Form.Item>
									</Col>
									<Col xs={24} lg={12}>
										<Form.Item
											name="lastName"
											rules={[
												{
													required: true,
													message: "Please input your Last Name!",
												},
											]}
										>
											<Input
												disabled={editMode !== "profile"}
												prefix={
													<UserOutlined className="site-form-item-icon" />
												}
												placeholder="Last Name"
											/>
										</Form.Item>
									</Col>
									<Col xs={24} lg={12}>
										<Form.Item
											name="emailId"
											rules={[
												{
													required: true,
													message: "Please input your email!",
												},
												{
													type: "email",
													message: "The input is not valid E-mail!",
												},
											]}
										>
											<Input
												disabled={true}
												prefix={
													<MailOutlined className="site-form-item-icon" />
												}
												placeholder="Email"
											/>
										</Form.Item>
									</Col>
								</Row>
								<Row gutter={48}>
									<Col xs={24} lg={12}>
										{editMode === "profile" ? (
											<Form.Item>
												<Button
													loading={updatingProfileDetails}
													type="primary"
													htmlType="submit"
													className="register-form-button"
												>
													Save Changes
												</Button>
											</Form.Item>
										) : (
											<Button
												onClick={() => handleChangeEditMode("profile")}
												className="back-btn"
											>
												Edit Profile
											</Button>
										)}
									</Col>
								</Row>
							</Form>
						</Panel>
						<Panel
							header="Password"
							key={2}
							className="site-collapse-custom-panel"
						>
							<Form
								name="normal_register"
								className="register-form"
								initialValues={{
									remember: true,
								}}
								onFinish={onFinish}
							>
								<Row gutter={48}>
									{editMode !== "password" ? (
										<Col xs={24} lg={12}>
											<Form.Item
												name="password"
												rules={[
													{
														required: true,
														message: "Please input your Password!",
													},
												]}
											>
												<Input
													disabled={editMode !== "password"}
													prefix={
														<LockOutlined className="site-form-item-icon" />
													}
													type="password"
													placeholder="Password"
												/>
											</Form.Item>
										</Col>
									) : (
										<>
											<Col xs={24} lg={12}>
												<Form.Item
													name="newPassword"
													rules={[
														{
															required: true,
															message: "Please input your Password!",
														},
													]}
												>
													<Input
														disabled={editMode !== "password"}
														prefix={
															<LockOutlined className="site-form-item-icon" />
														}
														type="password"
														placeholder="New Password"
													/>
												</Form.Item>
											</Col>
											<Col xs={24} lg={12}>
												<Form.Item
													name="oldPassword"
													rules={[
														{
															required: true,
															message: "Please input your Password!",
														},
													]}
												>
													<Input
														disabled={editMode !== "password"}
														prefix={
															<LockOutlined className="site-form-item-icon" />
														}
														type="password"
														placeholder="Old Password"
													/>
												</Form.Item>
											</Col>
										</>
									)}

									{editMode === "profile" && (
										<Col xs={24} lg={12}>
											<Form.Item
												name="newPassword"
												rules={[
													{
														required: true,
														message: "Please input your Password!",
													},
												]}
											>
												<Input
													disabled={editMode !== "password"}
													prefix={
														<LockOutlined className="site-form-item-icon" />
													}
													type="password"
													placeholder="New Password"
												/>
											</Form.Item>
										</Col>
									)}
								</Row>
								<Row gutter={48}>
									{editMode === "password" && (
										<Col xs={24} lg={12}>
											<Form.Item
												name="confirmPassword"
												rules={[
													{
														required: true,
														message: "Please input your Password!",
													},
												]}
											>
												<Input
													disabled={editMode !== "password"}
													prefix={
														<LockOutlined className="site-form-item-icon" />
													}
													type="password"
													placeholder="Confirm Password"
												/>
											</Form.Item>
										</Col>
									)}
								</Row>

								<Row gutter={48}>
									<Col xs={24} lg={12}>
										{editMode === "password" ? (
											<Form.Item>
												<Button
													type="primary"
													htmlType="submit"
													className="register-form-button"
												>
													Save Changes
												</Button>
											</Form.Item>
										) : (
											<Button
												onClick={() => handleChangeEditMode("password")}
												className="back-btn"
											>
												Change Password
											</Button>
										)}
									</Col>
								</Row>
							</Form>
						</Panel>
						<Panel
							header="Address"
							key={3}
							className="site-collapse-custom-panel"
						>
							<Form
								name="normal_register"
								className="register-form"
								initialValues={{
									remember: true,
								}}
								onFinish={onFinish}
							>
								<Row gutter={48}>
									{addresses?.map((address) => {
										return (
											<Col xs={24} lg={12}>
												<AddressCard
													data={address}
													showActions={true}
													onClickEdit={() =>
														router.push("/address/" + address._id)
													}
													onClickDelete={() => {}}
												/>
											</Col>
										);
									})}

									{/* <Col xs={24} lg={12}>
										<AddressCard
											showActions={true}
											onClickEdit={() =>
												router.push(
													'/address/scksdkcsdk'
												)
											}
											onClickDelete={() => {}}
										/>
									</Col>
									<Col xs={24} lg={12}>
										<AddressCard
											showActions={true}
											onClickEdit={() =>
												router.push(
													'/address/scksdkcsdk'
												)
											}
											onClickDelete={() => {}}
										/>
									</Col> */}
								</Row>
								<Row gutter={48}>
									<Col xs={24} lg={12}>
										<Form.Item>
											<Button
												className="back-btn"
												onClick={() => {
													router.push("/address/create");
												}}
											>
												Add New Address
											</Button>
										</Form.Item>
									</Col>
								</Row>
							</Form>
						</Panel>
						<Panel header="Settings">
							{/* <Button
								type="primary"
								onClick={() => {
									localStorage.removeItem("accessToken");
									message.success("Account Logged Out successfully");
									router.push("/login");
								}}
							>
								Logout
							</Button> */}

							<GoogleLogout
								render={(renderProps) => (
									<Button
										onClick={renderProps.onClick}
										disabled={renderProps.disabled}
										type="primary"
									>
										Logout
									</Button>
								)}
								clientId="897390064176-v7cgvd4pjhp0c6dsb035qcopojhfs0et.apps.googleusercontent.com"
								buttonText="Logout"
								onLogoutSuccess={() => {
									localStorage.removeItem("accessToken");
									message.success("Account Logged Out successfully");
									router.push("/login");
								}}
								onFailure={(err) => console.log(err)}
							></GoogleLogout>
							<Button
								type="primary"
								style={{marginLeft : 20}}
								onClick={() => {
									router.push("/track-order");
								}}
							>
								Track Order
							</Button>
						</Panel>
					</Collapse>
				</div>
			</Layout>
		</Wrapper>
	);
}

const Wrapper = styled.section`
	font-family: "Montserrat", sans-serif;
	width: 100vw;
	.container {
		width: 100%;
		display: flex;
		justify-content: center;
		align-items: center;
		flex-direction: column;
		/* background-color: ${Themes.backgroundMain}; */
	}
	.register-form {
		/* max-width: 350px; */
	}
	.register-form-forgot {
		float: right;
	}
	.ant-col-rtl .register-form-forgot {
		float: left;
	}
	.register-form-button {
		width: 100%;
	}
	.register-form-button {
		width: 100%;
	}
	[data-theme="compact"]
		.site-collapse-custom-collapse
		.site-collapse-custom-panel,
	.site-collapse-custom-collapse1 .site-collapse-custom-panel {
		overflow: hidden;
		background: #ffffff;
		border: 0px;
		border-radius: 2px;
		width: 90vw;
	}
	.back-btn {
		border: 1px solid ${Themes.primary};
		color: ${Themes.primary};
		width: 100%;
	}
`;
