import { call, select, put } from 'redux-saga/effects'
import request from '../../util/request'

import { API_URL } from '../../constants'
import {
	getCollectionsSuccess,
	getCollectionsFailure,
} from '../actions/collections'

export function * collectionsGet () {
	const state = yield select()
	try {
		const response = yield call(request, API_URL + `/guest/collections`, {
			method: 'GET',
			headers: {
				'Content-Type': 'application/json;charset=utf-8',
			}
		})
		if (response && response.apiStatus === 'SUCCESS') {
			yield put(getCollectionsSuccess(response.data))
		} else {
			yield put(getCollectionsFailure(response.meta.message))
		}
	} catch (error) {
		yield put(getCollectionsFailure(error.toString()))
	}
}

const CollectionsSaga = {
	collectionsGet,
}

export default CollectionsSaga
