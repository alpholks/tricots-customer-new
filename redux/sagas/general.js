import { call, select, put } from 'redux-saga/effects'
import request from '../../util/request'

import { API_URL } from '../../constants'
import {
	getBannersSuccess,
	getBannersFailure,

	getCollectionsSuccess,
	getCollectionsFailure,

	getProductsSuccess,
	getProductsFailure,
	getProductDetailsSuccess,
	getProductDetailsFailure,

	getHeadersSuccess,
	getHeadersFailure,

	getComboSuccess,
	getComboFailure,

	getComboDetailsSuccess,
	getComboDetailsFailure
} from '../actions/general'

export function * bannersGet () {
	const state = yield select()
	try {
		const response = yield call(request, API_URL + `/guest/banners`, {
			method: 'GET',
			headers: {
				'Content-Type': 'application/json;charset=utf-8',
			}
		})
		if (response && response.apiStatus === 'SUCCESS') {
			yield put(getBannersSuccess(response.data))
		} else {
			yield put(getBannersFailure(response.meta.message))
		}
	} catch (error) {
		yield put(getBannersFailure(error.toString()))
	}
}


export function * collectionsGet () {
	const state = yield select()
	try {
		const response = yield call(request, API_URL + `/guest/collections`, {
			method: 'GET',
			headers: {
				'Content-Type': 'application/json;charset=utf-8',
			}
		})
		if (response && response.apiStatus === 'SUCCESS') {
			yield put(getCollectionsSuccess(response.data))
		} else {
			yield put(getCollectionsFailure(response.meta.message))
		}
	} catch (error) {
		yield put(getCollectionsFailure(error.toString()))
	}
}

export function * productsGet ({collectionId}) {
	var token =  localStorage.getItem("accessToken")
	var role = "guest"
	if(token){
		role = "customer"
	}
	var filterParams=  collectionId === ("isNewArrival"|| "bestSale" ||"isWholeSale" ||"isFeatured") ? `${collectionId}=${true}` : `slug=${collectionId}`

	try {
		const response = yield call(request, API_URL + `/${role}/products?`+ filterParams, {
			method: 'GET',
			headers: {
				'Content-Type': 'application/json;charset=utf-8',
				"Authorization": "jwt "+token
			}
		})
		if (response && response.apiStatus === 'SUCCESS') {
			yield put(getProductsSuccess(response.data))
		} else {
			yield put(getProductsFailure(response.meta.message))
		}
	} catch (error) {
		yield put(getProductsFailure(error.toString()))
	}
}


export function * comboGet () {
	var token =  localStorage.getItem("accessToken")
	try {
		const response = yield call(request, API_URL + `/guest/combos`, {
			method: 'GET',
			headers: {
				'Content-Type': 'application/json;charset=utf-8',
				"Authorization": "jwt "+token
			}
		})
		if (response && response.apiStatus === 'SUCCESS') {
			yield put(getComboSuccess(response.data))
		} else {
			yield put(getComboFailure(response.meta.message))
		}
	} catch (error) {
		yield put(getComboFailure(error.toString()))
	}
}


export function * comboDetailsGet ({comboId}) {
	var token =  localStorage.getItem("accessToken")
	try {
		const response = yield call(request, API_URL + `/guest/combos/`+ comboId, {
			method: 'GET',
			headers: {
				'Content-Type': 'application/json;charset=utf-8',
				"Authorization": "jwt "+token
			}
		})
		if (response && response.apiStatus === 'SUCCESS') {
			yield put(getComboDetailsSuccess(response.data))
		} else {
			yield put(getComboDetailsFailure(response.meta.message))
		}
	} catch (error) {
		yield put(getComboDetailsFailure(error.toString()))
	}
}

export function * productDetailsGet ({productId}) {
	var token =  localStorage.getItem("accessToken")
	var role = "guest"
	if(token){
		role = "customer"
	}

	try {
		const response = yield call(request, API_URL + `/${role}/products/` +productId, {
			method: 'GET',
			headers: {
				'Content-Type': 'application/json;charset=utf-8',
				"Authorization": "jwt "+token
			}
		})
		if (response && response.apiStatus === 'SUCCESS') {
			yield put(getProductDetailsSuccess(response.data))
		} else {
			yield put(getProductDetailsFailure(response.meta.message))
		}
	} catch (error) {
		yield put(getProductDetailsFailure(error.toString()))
	}
}


export function * headersGet () {
	const state = yield select()
	try {
		const response = yield call(request, API_URL + `/guest/headers`, {
			method: 'GET',
			headers: {
				'Content-Type': 'application/json;charset=utf-8',
			}
		})
		if (response && response.apiStatus === 'SUCCESS') {
			yield put(getHeadersSuccess(response.data))
		} else {
			yield put(getHeadersFailure(response.meta.message))
		}
	} catch (error) {
		yield put(getHeadersFailure(error.toString()))
	}
}

const generalSaga = {
	bannersGet,
	collectionsGet,
	productsGet,
	comboGet,
	comboDetailsGet,
	productDetailsGet,
	headersGet
}

export default generalSaga
