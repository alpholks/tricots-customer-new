import { call, select, put } from 'redux-saga/effects'
import request from '../../util/request'

import { API_URL } from '../../constants'
import {
	getBannersSuccess,
	getBannersFailure,
} from '../actions/banners'

export function * bannersGet () {
	const state = yield select()
	try {
		const response = yield call(request, API_URL + `/guest/banners`, {
			method: 'GET',
			headers: {
				'Content-Type': 'application/json;charset=utf-8',
			}
		})
		if (response && response.apiStatus === 'SUCCESS') {
			yield put(getBannersSuccess(response.data))
		} else {
			yield put(getBannersFailure(response.meta.message))
		}
	} catch (error) {
		yield put(getBannersFailure(error.toString()))
	}
}

const BannersSaga = {
	bannersGet,
}

export default BannersSaga
