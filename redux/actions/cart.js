export const actionTypes = {
	CLEAR_ERROR: 'CART/CLEAR_ERROR',
	STATE_RESET: 'CART/STATE_RESET',

	CREATE_CART_REQUEST: 'CART/CREATE_CART_REQUEST',
	CREATE_CART_SUCCESS: 'CART/CREATE_CART_SUCCESS',
    CREATE_CART_FAILURE: 'CART/CREATE_CART_FAILURE',

    ADD_TO_CART_REQUEST: 'CART/ADD_TO_CART_REQUEST',
	ADD_TO_CART_SUCCESS: 'CART/ADD_TO_CART_SUCCESS',
    ADD_TO_CART_FAILURE: 'CART/ADD_TO_CART_FAILURE',

    REMOVE_FROM_CART_REQUEST: 'CART/REMOVE_FROM_CART_REQUEST',
	REMOVE_FROM_CART_SUCCESS: 'CART/REMOVE_FROM_CART_SUCCESS',
    REMOVE_FROM_CART_FAILURE: 'CART/REMOVE_FROM_CART_FAILURE',

    DELETE_CART_REQUEST: 'CART/DELETE_CART_REQUEST',
	DELETE_CART_SUCCESS: 'CART/DELETE_CART_SUCCESS',
    DELETE_CART_FAILURE: 'CART/DELETE_CART_FAILURE',

    EMPTY_CART_REQUEST: 'CART/EMPTY_CART_REQUEST',
	EMPTY_CART_SUCCESS: 'CART/EMPTY_CART_SUCCESS',
    EMPTY_CART_FAILURE: 'CART/EMPTY_CART_FAILURE',
}

export const clearError = () => ({ type: actionTypes.CLEAR_ERROR })
export const stateReset = (resetState) => ({ type: actionTypes.STATE_RESET, resetState })

export const createCartRequest = (data) => ({ type: actionTypes.CREATE_CART_REQUEST, data })
export const createCartFailure = (error) => ({ type: actionTypes.CREATE_CART_FAILURE, error })
export const createCartSuccess = (cart) => ({ type: actionTypes.CREATE_CART_SUCCESS, cart })

export const addToCartRequest = (inventoryId, quantity) => ({ type: actionTypes.ADD_TO_CART_REQUEST, inventoryId, quantity })
export const addToCartFailure = (error) => ({ type: actionTypes.ADD_TO_CART_FAILURE, error })
export const addToCartSuccess = (cart) => ({ type: actionTypes.ADD_TO_CART_SUCCESS, cart })

export const removeFromCartRequest = (inventoryId) => ({ type: actionTypes.REMOVE_FROM_CART_REQUEST, inventoryId })
export const removeFromCartFailure = (error) => ({ type: actionTypes.REMOVE_FROM_CART_FAILURE, error })
export const removeFromCartSuccess = (cart) => ({ type: actionTypes.REMOVE_FROM_CART_SUCCESS, cart })

export const deleteCartRequest = (inventoryId) => ({ type: actionTypes.DELETE_CART_REQUEST,inventoryId  })
export const deleteCartFailure = (error) => ({ type: actionTypes.DELETE_CART_FAILURE, error })
export const deleteCartSuccess = (cart) => ({ type: actionTypes.DELETE_CART_SUCCESS,cart })

export const emptyCartRequest = () => ({ type: actionTypes.EMPTY_CART_REQUEST,  })
export const emptyCartFailure = (error) => ({ type: actionTypes.EMPTY_CART_FAILURE, error })
export const emptyCartSuccess = (cart) => ({ type: actionTypes.EMPTY_CART_SUCCESS,cart })

const AuthActions = {
	actionTypes,

	clearError,
	stateReset,

    createCartRequest,
    createCartFailure,
    createCartSuccess,

    addToCartRequest,
    addToCartFailure,
    addToCartSuccess,

    deleteCartRequest,
    deleteCartFailure,
    deleteCartSuccess,

    removeFromCartRequest,
    removeFromCartFailure,
    removeFromCartSuccess,

    emptyCartFailure,
    emptyCartRequest,
    emptyCartSuccess

}

export default AuthActions
