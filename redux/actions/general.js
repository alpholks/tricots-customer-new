export const actionTypes = {
	CLEAR_ERROR: 'GENERAL/CLEAR_ERROR',
    STATE_RESET: 'GENERAL/STATE_RESET',

	GET_BANNERS_REQUEST: 'GENERAL/GET_BANNERS_REQUEST',
	GET_BANNERS_SUCCESS: 'GENERAL/GET_BANNERS_SUCCESS',
    GET_BANNERS_FAILURE: 'GENERAL/GET_BANNERS_FAILURE',

	GET_COLLECTIONS_REQUEST: 'GENERAL/GET_COLLECTIONS_REQUEST',
	GET_COLLECTIONS_SUCCESS: 'GENERAL/GET_COLLECTIONS_SUCCESS',
    GET_COLLECTIONS_FAILURE: 'GENERAL/GET_COLLECTIONS_FAILURE',

	GET_PRODUCTS_REQUEST: 'GENERAL/GET_PRODUCTS_REQUEST',
	GET_PRODUCTS_SUCCESS: 'GENERAL/GET_PRODUCTS_SUCCESS',
    GET_PRODUCTS_FAILURE: 'GENERAL/GET_PRODUCTS_FAILURE',

    GET_COMBO_REQUEST: 'GENERAL/GET_COMBO_REQUEST',
	GET_COMBO_SUCCESS: 'GENERAL/GET_COMBO_SUCCESS',
    GET_COMBO_FAILURE: 'GENERAL/GET_COMBO_FAILURE',

    GET_COMBO_DETAILS_REQUEST: 'GENERAL/GET_COMBO_DETAILS_REQUEST',
	GET_COMBO_DETAILS_SUCCESS: 'GENERAL/GET_COMBO_DETAILS_SUCCESS',
    GET_COMBO_DETAILS_FAILURE: 'GENERAL/GET_COMBO_DETAILS_FAILURE',

    GET_PRODUCT_DETAILS_REQUEST: 'GENERAL/GET_PRODUCT_DETAILS_REQUEST',
	GET_PRODUCT_DETAILS_SUCCESS: 'GENERAL/GET_PRODUCT_DETAILS_SUCCESS',
    GET_PRODUCT_DETAILS_FAILURE: 'GENERAL/GET_PRODUCT_DETAILS_FAILURE',

	GET_HEADERS_REQUEST: 'HEADERS/GET_HEADERS_REQUEST',
	GET_HEADERS_SUCCESS: 'HEADERS/GET_HEADERS_SUCCESS',
    GET_HEADERS_FAILURE: 'HEADERS/GET_HEADERS_FAILURE',
}

export const clearError = () => ({ type: actionTypes.CLEAR_ERROR })
export const stateReset = (resetState) => ({ type: actionTypes.STATE_RESET, resetState })

export const getBannersRequest = () => ({ type: actionTypes.GET_BANNERS_REQUEST  })
export const getBannersFailure = (error) => ({ type: actionTypes.GET_BANNERS_FAILURE, error })
export const getBannersSuccess = (banners) => ({ type: actionTypes.GET_BANNERS_SUCCESS, banners })

export const getProductsRequest = (collectionId) => ({ type: actionTypes.GET_PRODUCTS_REQUEST, collectionId  })
export const getProductsFailure = (error) => ({ type: actionTypes.GET_PRODUCTS_FAILURE, error })
export const getProductsSuccess = (products) => ({ type: actionTypes.GET_PRODUCTS_SUCCESS, products })

export const getComboRequest = () => ({ type: actionTypes.GET_COMBO_REQUEST,   })
export const getComboFailure = (error) => ({ type: actionTypes.GET_COMBO_FAILURE, error })
export const getComboSuccess = (combo) => ({ type: actionTypes.GET_COMBO_SUCCESS, combo })

export const getComboDetailsRequest = (comboId) => ({ type: actionTypes.GET_COMBO_DETAILS_REQUEST,  comboId })
export const getComboDetailsFailure = (error) => ({ type: actionTypes.GET_COMBO_DETAILS_FAILURE, error })
export const getComboDetailsSuccess = (comboDetails) => ({ type: actionTypes.GET_COMBO_DETAILS_SUCCESS, comboDetails })

export const getProductDetailsRequest = (productId) => ({ type: actionTypes.GET_PRODUCT_DETAILS_REQUEST, productId  })
export const getProductDetailsFailure = (error) => ({ type: actionTypes.GET_PRODUCT_DETAILS_FAILURE, error })
export const getProductDetailsSuccess = (productDetails) => ({ type: actionTypes.GET_PRODUCT_DETAILS_SUCCESS, productDetails })

export const getCollectionsRequest = () => ({ type: actionTypes.GET_COLLECTIONS_REQUEST  })
export const getCollectionsFailure = (error) => ({ type: actionTypes.GET_COLLECTIONS_FAILURE, error })
export const getCollectionsSuccess = (collections) => ({ type: actionTypes.GET_COLLECTIONS_SUCCESS, collections })

export const getHeadersRequest = () => ({ type: actionTypes.GET_HEADERS_REQUEST})
export const getHeadersFailure = (error) => ({ type: actionTypes.GET_HEADERS_FAILURE, error })
export const getHeadersSuccess = (headers) => ({ type: actionTypes.GET_HEADERS_SUCCESS, headers })

const generalActions = {
	actionTypes,

	clearError,
	stateReset,

    getBannersRequest,
    getBannersFailure,
    getBannersSuccess,

    getProductsRequest,
    getProductsFailure,
    getProductsSuccess,

    getComboRequest,
    getComboFailure,
    getComboSuccess,

    getComboDetailsRequest,
    getComboDetailsFailure,
    getComboDetailsSuccess,

    getCollectionsRequest,
    getCollectionsFailure,
    getCollectionsSuccess,

    getHeadersRequest,
    getHeadersFailure,
    getHeadersSuccess,

    getProductDetailsFailure,
    getProductDetailsRequest,
    getProductDetailsSuccess

}

export default generalActions
