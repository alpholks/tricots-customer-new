export const actionTypes = {
	CLEAR_ERROR: 'COMBO_CART/CLEAR_ERROR',
	STATE_RESET: 'COMBO_CART/STATE_RESET',

	CREATE_COMBO_CART_REQUEST: 'COMBO_CART/CREATE_COMBO_CART_REQUEST',
	CREATE_COMBO_CART_SUCCESS: 'COMBO_CART/CREATE_COMBO_CART_SUCCESS',
    CREATE_COMBO_CART_FAILURE: 'COMBO_CART/CREATE_COMBO_CART_FAILURE',

    ADD_TO_COMBO_CART_REQUEST: 'COMBO_CART/ADD_TO_COMBO_CART_REQUEST',
	ADD_TO_COMBO_CART_SUCCESS: 'COMBO_CART/ADD_TO_COMBO_CART_SUCCESS',
    ADD_TO_COMBO_CART_FAILURE: 'COMBO_CART/ADD_TO_COMBO_CART_FAILURE',

    REMOVE_FROM_COMBO_CART_REQUEST: 'COMBO_CART/REMOVE_FROM_COMBO_CART_REQUEST',
	REMOVE_FROM_COMBO_CART_SUCCESS: 'COMBO_CART/REMOVE_FROM_COMBO_CART_SUCCESS',
    REMOVE_FROM_COMBO_CART_FAILURE: 'COMBO_CART/REMOVE_FROM_COMBO_CART_FAILURE',

    DELETE_COMBO_CART_REQUEST: 'COMBO_CART/DELETE_COMBO_CART_REQUEST',
	DELETE_COMBO_CART_SUCCESS: 'COMBO_CART/DELETE_COMBO_CART_SUCCESS',
    DELETE_COMBO_CART_FAILURE: 'COMBO_CART/DELETE_COMBO_CART_FAILURE',

    EMPTY_COMBO_CART_REQUEST: 'COMBO_CART/EMPTY_COMBO_CART_REQUEST',
	EMPTY_COMBO_CART_SUCCESS: 'COMBO_CART/EMPTY_COMBO_CART_SUCCESS',
    EMPTY_COMBO_CART_FAILURE: 'COMBO_CART/EMPTY_COMBO_CART_FAILURE',
}

export const clearError = () => ({ type: actionTypes.CLEAR_ERROR })
export const stateReset = (resetState) => ({ type: actionTypes.STATE_RESET, resetState })

export const createComboCartRequest = (data) => ({ type: actionTypes.CREATE_COMBO_CART_REQUEST, data })
export const createComboCartFailure = (error) => ({ type: actionTypes.CREATE_COMBO_CART_FAILURE, error })
export const createComboCartSuccess = (comboCart) => ({ type: actionTypes.CREATE_COMBO_CART_SUCCESS, comboCart })

export const addToComboCartRequest = (inventoryId, quantity) => ({ type: actionTypes.ADD_TO_COMBO_CART_REQUEST, inventoryId, quantity })
export const addToComboCartFailure = (error) => ({ type: actionTypes.ADD_TO_COMBO_CART_FAILURE, error })
export const addToComboCartSuccess = (comboCart) => ({ type: actionTypes.ADD_TO_COMBO_CART_SUCCESS, comboCart })

export const removeFromComboCartRequest = (inventoryId) => ({ type: actionTypes.REMOVE_FROM_COMBO_CART_REQUEST, inventoryId })
export const removeFromComboCartFailure = (error) => ({ type: actionTypes.REMOVE_FROM_COMBO_CART_FAILURE, error })
export const removeFromComboCartSuccess = (comboCart) => ({ type: actionTypes.REMOVE_FROM_COMBO_CART_SUCCESS, comboCart })

export const deleteComboCartRequest = (inventoryId) => ({ type: actionTypes.DELETE_COMBO_CART_REQUEST,inventoryId  })
export const deleteComboCartFailure = (error) => ({ type: actionTypes.DELETE_COMBO_CART_FAILURE, error })
export const deleteComboCartSuccess = (comboCart) => ({ type: actionTypes.DELETE_COMBO_CART_SUCCESS,comboCart })

export const emptyComboCartRequest = () => ({ type: actionTypes.EMPTY_COMBO_CART_REQUEST,  })
export const emptyComboCartFailure = (error) => ({ type: actionTypes.EMPTY_COMBO_CART_FAILURE, error })
export const emptyComboCartSuccess = (comboCart) => ({ type: actionTypes.EMPTY_COMBO_CART_SUCCESS,comboCart })

const ComboCartActions = {
	actionTypes,

	clearError,
	stateReset,

    createComboCartRequest,
    createComboCartFailure,
    createComboCartSuccess,

    addToComboCartRequest,
    addToComboCartFailure,
    addToComboCartSuccess,

    deleteComboCartRequest,
    deleteComboCartFailure,
    deleteComboCartSuccess,

    removeFromComboCartRequest,
    removeFromComboCartFailure,
    removeFromComboCartSuccess,

    emptyComboCartRequest,
    emptyComboCartFailure,
    emptyComboCartSuccess

}

export default ComboCartActions
