export const actionTypes = {
	CLEAR_ERROR: 'BANNERS/CLEAR_ERROR',
	STATE_RESET: 'BANNERS/STATE_RESET',

	GET_BANNERS_REQUEST: 'BANNERS/GET_BANNERS_REQUEST',
	GET_BANNERS_SUCCESS: 'BANNERS/GET_BANNERS_SUCCESS',
    GET_BANNERS_FAILURE: 'BANNERS/GET_BANNERS_FAILURE',
}

export const clearError = () => ({ type: actionTypes.CLEAR_ERROR })
export const stateReset = (resetState) => ({ type: actionTypes.STATE_RESET, resetState })

export const getBannersRequest = () => ({ type: actionTypes.GET_BANNERS_REQUEST  })
export const getBannersFailure = (error) => ({ type: actionTypes.GET_BANNERS_FAILURE, error })
export const getBannersSuccess = (banners) => ({ type: actionTypes.GET_BANNERS_SUCCESS, banners })

const BannerActions = {
	actionTypes,

	clearError,
	stateReset,

    getBannersRequest,
    getBannersFailure,
    getBannersSuccess,

}

export default BannerActions
