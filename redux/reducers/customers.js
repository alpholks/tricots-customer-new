import { actionTypes } from '../actions/customers'

export const initialState = {

    fetchingCustomers: false,
    fetchCustomerSuccessful: false,
    customers: [],

    fetchingCustomerDetails: false,
    fetchCustomerDetailsSuccessful: false,
    customerDetails: null,

    creatingCustomer: false,
    createCustomerSuccessful: false,
    customer: null,

    updatingCustomer: false,
    updateCustomerSuccessful: false,

    deletingCustomer: false,
    deleteCustomerSuccessful: false,

	error: null
}

function customerReducer (state = initialState, action) {
	switch (action.type) {
		case actionTypes.CLEAR_ERROR: return { ...state, ...{ error: null } }
		case actionTypes.STATE_RESET: return { ...state, ...action.resetState }

		case actionTypes.GET_CUSTOMERS_REQUEST: return { ...state, ...{ fetchingCustomers: true } }
		case actionTypes.GET_CUSTOMERS_FAILURE: return { ...state, ...{ fetchingCustomers: false, error: action.error } }
		case actionTypes.GET_CUSTOMERS_SUCCESS: return { ...state, ...{ fetchingCustomers: false, fetchCustomerSuccessful: true, customers: action.customers } }

        case actionTypes.GET_CUSTOMER_DETAILS_REQUEST: return { ...state, ...{ fetchingCustomerDetails: true } }
		case actionTypes.GET_CUSTOMER_DETAILS_FAILURE: return { ...state, ...{ fetchingCustomerDetails: false, error: action.error } }
		case actionTypes.GET_CUSTOMER_DETAILS_SUCCESS: return { ...state, ...{ fetchingCustomerDetails: false, fetchCustomerDetailsSuccessful: true, customerDetails: action.customerDetails } }

        case actionTypes.CREATE_CUSTOMER_REQUEST: return { ...state, ...{ creatingCustomer: true } }
		case actionTypes.CREATE_CUSTOMER_FAILURE: return { ...state, ...{ creatingCustomer: false, error: action.error } }
		case actionTypes.CREATE_CUSTOMER_SUCCESS: return { ...state, ...{ creatingCustomer: false, createCustomerSuccessful: true, customer: action.customer } }

        case actionTypes.UPDATE_CUSTOMER_REQUEST: return { ...state, ...{ updatingCustomer: true } }
		case actionTypes.UPDATE_CUSTOMER_FAILURE: return { ...state, ...{ updatingCustomer: false, error: action.error } }
        case actionTypes.UPDATE_CUSTOMER_SUCCESS: return { ...state, ...{ updatingCustomer: false, updateCustomerSuccessful: true, customer: action.customer } }

        case actionTypes.DELETE_CUSTOMER_REQUEST: return { ...state, ...{ deletingCustomer: true } }
		case actionTypes.DELETE_CUSTOMER_FAILURE: return { ...state, ...{ deletingCustomer: false, error: action.error } }
        case actionTypes.DELETE_CUSTOMER_SUCCESS: return { ...state, ...{ deletingCustomer: false, deleteCustomerSuccessful: true, customer: null } }

		default:return state
	}
}

export default customerReducer
