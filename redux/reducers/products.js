import { actionTypes } from '../actions/products'

export const initialState = {

    fetchingProducts: false,
    fetchProductsSuccessful: false,
    products: [],

	error: null
}

function productsReducer (state = initialState, action) {
	switch (action.type) {
		case actionTypes.CLEAR_ERROR: return { ...state, ...{ error: null } }
		case actionTypes.STATE_RESET: return { ...state, ...action.resetState }

		case actionTypes.GET_PRODUCTS_REQUEST: return { ...state, ...{ fetchingProducts: true } }
		case actionTypes.GET_PRODUCTS_FAILURE: return { ...state, ...{ fetchingProducts: false, error: action.error } }
		case actionTypes.GET_PRODUCTS_SUCCESS: return { ...state, ...{ fetchingProducts: false, fetchProductsSuccessful: true, products: action.products } }

		default:return state
	}
}

export default productsReducer
