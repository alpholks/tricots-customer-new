import { actionTypes } from '../actions/wishList'

export const initialState = {

    addingToWishList: false,
    addToWishListSuccessful: false,
    wishList: [],

    removingFromWishListDetails: false,
    removeFromWishListSuccessful: false,

    fetchingWishList: false,
    fetchWishListSuccessful: false,
    
	error: null
}

function wishListReducer (state = initialState, action) {
	switch (action.type) {
		case actionTypes.CLEAR_ERROR: return { ...state, ...{ error: null } }
		case actionTypes.STATE_RESET: return { ...state, ...action.resetState }

		case actionTypes.ADD_TO_WISHLIST_REQUEST: return { ...state, ...{ addingToWishList: true } }
		case actionTypes.ADD_TO_WISHLIST_FAILURE: return { ...state, ...{ addingToWishList: false, error: action.error } }
		case actionTypes.ADD_TO_WISHLIST_SUCCESS: return { ...state, ...{ addingToWishList: false, addToWishListSuccessful: true, wishList: action.wishList } }

        case actionTypes.REMOVE_FROM_WISHLIST_REQUEST: return { ...state, ...{ removingFromWishListDetails: true } }
		case actionTypes.REMOVE_FROM_WISHLIST_FAILURE: return { ...state, ...{ removingFromWishListDetails: false, error: action.error } }
		case actionTypes.REMOVE_FROM_WISHLIST_SUCCESS: return { ...state, ...{ removingFromWishListDetails: false, removeFromWishListSuccessful: true, wishList: action.wishList } }

        case actionTypes.GET_WISHLIST_REQUEST: return { ...state, ...{ fetchingWishList: true } }
		case actionTypes.GET_WISHLIST_FAILURE: return { ...state, ...{ fetchingWishList: false, error: action.error } }
        case actionTypes.GET_WISHLIST_SUCCESS: return { ...state, ...{ fetchingWishList: false, fetchWishListSuccessful: true, wishListProductDetails: action.wishListProductDetails } }

		default:return state
	}
}

export default wishListReducer
