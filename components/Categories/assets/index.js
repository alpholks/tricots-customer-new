const Images = {
    category1: require('./images/category1.jpg'),
    category2: require('./images/category.jpg'),
    category3: require('./images/category3.jpg'),
    category4: require('./images/category4.jpg'),
    category5: require('./images/category5.jpg'),
    category6: require('./images/category6.jpg')
}
export {
	Images
}
const ComponentAssets = {
	Images
}
export default ComponentAssets
