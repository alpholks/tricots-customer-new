import PropTypes from "prop-types";
import HeaderWrapper from "./Header.style";
import { Images } from "./assets/index";
import styled from "styled-components";
import React, { useEffect, useState } from "react";
import {
	Row,
	Col,
	Space,
	Grid,
	Dropdown,
	Drawer,
	Button,
	Badge,
	Avatar,
} from "antd";
import {
	HeartFilled,
	ShoppingCartOutlined,
	MenuOutlined,
	WhatsAppOutlined,
	EnvironmentOutlined,
	UserOutlined,
} from "@ant-design/icons";
import { useRouter } from "next/router";
import { useDispatch, useSelector } from "react-redux";
import generalActions from "../../redux/actions/general";

const { useBreakpoint } = Grid;
import { Menu } from "antd";

const { SubMenu } = Menu;
const MobileHeader = ({ header, router }) => {
	return (
		<Menu
			onClick={() => {}}
			style={{
				width: 240,
				marginLeft: -20,
				color: "#000",
				border: "none",
			}}
			// defaultSelectedKeys={["1"]}
			// defaultOpenKeys={["sub1"]}
			mode="vertical-right"
		>
			{header?.subMenu?.length !== 0 && header?.title !== "Combo" ? (
				<SubMenu
					key="sub1"
					// icon={<MailOutlined />}
					title={header.title}
				>
					{header?.subMenu?.map((sb, index) => {
						return (
								<Menu.Item
									key={index}
									onClick={() => router.push("/" + sb.collectionId?.slug)}
								>
									{sb.name}{" "}
								</Menu.Item>
						);
					})}
				</SubMenu>
			) : (
				<Menu.Item key="4" onClick={() => router.push("/combo")}>
					{header.title}
				</Menu.Item>
			)}
		</Menu>
	);
};
const MenuComponent = ({ subMenu, router }) => {
	return (
		<Menu>
			{subMenu?.map((sm) => {
				return (
					sm.type !== "combo" && (
						<Menu.Item>
							{sm.type === "collection" ? (
								<div onClick={() => router.push("/" + sm.collectionId?.slug)}>
									{sm.name}
								</div>
							) : (
								<div onClick={() => router.push("/" + sm.collectionId?.slug)}>
									{sm.name}
								</div>
							)}
						</Menu.Item>
					)
				);
			})}
		</Menu>
	);
};

export default function Header(props) {
	const dispatch = useDispatch();
	const router = useRouter();
	const { headers } = useSelector((state) => state.general);
	const { cart } = useSelector((state) => state.cart);
	const { comboCart } = useSelector((state) => state.comboCart);

	const [visible, setVisible] = useState(false);
	const [placement, setPlacement] = useState("left");
	const [logoUrl, setLogoUrl] = useState("");
	const [admin, setAdmin] = useState("");
	const showDrawer = () => {
		setVisible(true);
	};

	const onClose = () => {
		setVisible(false);
	};

	const handleNavigation = (route) => {
		router.push(route);
	};
	const screens = useBreakpoint();
	const getAdmin = () => {
		let admin = JSON.parse(localStorage.getItem("admin"));
		console.log(admin);
		setLogoUrl(admin?.logoUrl);
		setAdmin(admin);
	};
	useEffect(() => {
		getAdmin();
		dispatch(generalActions.getHeadersRequest());
	}, []);

	return (
		<HeaderWrapper>
			{screens.xs && (
				<Drawer
					className="menu"
					title={
						<div>
							<img
								src={logoUrl}
								onClick={() => {
									router.push(admin?.website);
								}}
								style={{ width: 150, height: "6vh" }}
							/>
						</div>
					}
					placement={placement}
					closable={false}
					onClose={onClose}
					visible={visible}
					key={placement}
				>
					{headers.map((header) => {
						return <MobileHeader header={header} router={router} />;
					})}
					{/* <p className="menu-item">Men</p>
					<p className="menu-item">Women</p>
					<p className="menu-item">Couple's tshirts</p>
					<p className="menu-item">Combos</p>
					<p className="menu-item">Boxers</p> */}
				</Drawer>
			)}
			<div className="track-order-container">
				<div className="track-order">
					<span
						className="top-header-text"
						onClick={() => handleNavigation("/track-order")}
					>
						<EnvironmentOutlined /> Track order
					</span>
					{localStorage.getItem("accessToken") ? (
						<div className="top-header-text">
							<span onClick={() => handleNavigation("/login")}>
							<WhatsAppOutlined />	{admin.whatsapp}
							</span>
						</div>
					) : (
						<div className="top-header-text">
							<span onClick={() => handleNavigation("/login")}>
								Login{"  "}
							</span>
							<span onClick={() => handleNavigation("/signup")}>| Signup</span>
						</div>
					)}
				</div>
			</div>
			<div className="top-header-container">
				<div className="top-header">
					{/* <img src={Images.logo} width={180} /> */}
					{screens.xs && (
						<MenuOutlined
							style={{ fontSize: 20 }}
							onClick={() => setVisible(true)}
						/>
					)}
					<img
						src={logoUrl}
						onClick={() => {
							router.push(admin.website);
						}}
						style={{ width: 150, height: "6vh", cursor: "pointer" }}
					/>
					{!screens.xs && (
						<Space size="large">
							{headers?.map((header) => {
								return header.title !== "Combo" ? (
									<Dropdown
										overlay={
											<MenuComponent subMenu={header.subMenu} router={router} />
										}
									>
										<Button
											type="primary"
											className="header-text"
											onClick={(e) => e.preventDefault()}
										>
											{header.title}
										</Button>
									</Dropdown>
								) : (
									<Dropdown
										overlay={() => (
											<MenuComponent subMenu={header.subMenu} router={router} />
										)}
									>
										<Button
											type="primary"
											className="header-text"
											onClick={(e) => {
												e.preventDefault();
												router.push("/combo");
											}}
										>
											{header.title}
										</Button>
									</Dropdown>
								);
							})}
						</Space>
					)}
					<Space size={20}>
						{localStorage.getItem("accessToken") ? (
							<>
								<UserOutlined
									style={{
										border: "1px solid #000",
										padding: 5,
										borderRadius: 25,
									}}
									onClick={() => router.push("/profile")}
								/>
								<HeartFilled
									className="header-icons"
									onClick={() => router.push("/wishlist")}
								/>
							</>
						) : null}
						<Badge
							count={Object.keys(cart).length + Object.keys(comboCart).length}
							size={"small"}
						>
							<ShoppingCartOutlined
								className="header-icons"
								onClick={() => router.push("/cart")}
							/>
						</Badge>
					</Space>
				</div>
			</div>
		</HeaderWrapper>
	);
}
