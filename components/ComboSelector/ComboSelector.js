import ComboSelectorWrapper from "./ComboSelector.style";
import React, { useState } from "react";
import { Images } from "./assets";
import { Row, Col, Form, Input, Button, Space, Select } from "antd";
import {
	HeartOutlined,
	MinusCircleOutlined,
	PlusOutlined,
} from "@ant-design/icons";

const { Option } = Select;
export function ComboSelector(props) {
	const { colors, sizes, handleChangeSize, handleChangeColor, data } = props;

	const [selectedColor, setSelectedColor] = useState("");
	const [selectedSize, setSelectedsize] = useState("");

	return (
		<ComboSelectorWrapper>
			<Row
				gutter={[12, 24]}
				className="combo-item"
				justify="space-between"
				align="middle"
			>
				<Col xs={12} sm={12} md={12} l={12} xl={12}>
					<Select
						style={{ width: "100%" }}
						onChange={(value) => {
							setSelectedColor(value);
							handleChangeColor(value);
						}}
						placeholder="Color"
					>
						{colors?.map((color) => {
							return (
								<Option value={color?.color}>
									<div
										style={{
											display: "flex",
											flexDirection: "row",
											justifyContent: "space-evenly",
											alignItems: "center",
										}}
									>
										<div
											style={{
												width: "15px",
												height: "15px",
												backgroundColor: color?.color,
												borderRadius: "50px",
											}}
										></div>
										{color?.colorName}
									</div>
								</Option>
							);
						})}
					</Select>
				</Col>
				<Col xs={12} sm={12} md={12} l={12} xl={12}>
					<Select
						style={{ width: "100%" }}
						onChange={(value) => {
							setSelectedsize(value);
							handleChangeSize(selectedColor, value);
						}}
						placeholder="Size"
					>
						{sizes?.map((size) => {
							var value = data?.filter((inv) => {
								if (inv.size === size && inv.color === selectedColor && inv.stock !==0) {
									return size;
								}
							});
							if (value.length !== 0) {
								return <Option value={size}>{size}</Option>;
							} else {
								return (
									<Option disabled value={size}>
										{`${size} (out of stock)`}
									</Option>
								);
							}
						})}
					</Select>
				</Col>
			</Row>
		</ComboSelectorWrapper>
	);
}

export default ComboSelector;
