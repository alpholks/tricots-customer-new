import styled from 'styled-components'
import Theme from '../../themes/themes'

const Wrapper = styled.section`
	font-family: "Montserrat", sans-serif;
	display:flex;
	flex-direction : column;
	justify-content:center;
	align-items : center;
	margin-bottom: 10px;
	.address-name {
		font-size:14px;
		font-weight:600;
	}

`

export default Wrapper
