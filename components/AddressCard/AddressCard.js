import AddressCardWrapper from './AddressCard.style'
import React, { useState } from 'react'
import { Images } from './assets'
import { Row, Col, Checkbox, Radio } from 'antd'
import { EditOutlined, DeleteOutlined } from '@ant-design/icons'

export function CartItem (props) {
	const { showRadio, showActions, onClickEdit, onClickDelete, data, onSelectAddress, checked } = props
	return (
		<AddressCardWrapper>
			<Row
				gutter={0}
				align="middle"
				style={{
					width: '100%',
					borderRadius: '5px',
					// boxShadow:
					// 	"0 2px 2px 0 rgba(0, 0, 0, 0.2), 0 2px 2px 0px rgba(0, 0, 0, 0.19)",
					padding: '10px',
					border: '1px solid #000'
				}}
			>
				{showRadio && (
					<Col xs={2} sm={2} md={2} l={2} xl={2}>
						<Radio checked={checked} onClick={() =>onSelectAddress(data)}/>
					</Col>
				)}
				<Col xs={20} sm={20} md={20} l={20} xl={20}>
					<div className="address-name">
						<span className="special-address-text">{data?.type}</span>
					</div>
					<span>{data?.doorNumber + ",  "}</span>
					<span>{data?.street + ",  "} </span>
					<span>{data?.landmark + ",  "} </span>
					<span>{data?.district + ",  "}</span>
					<span>{data?.pincode + ",  "}</span>
					<div>{data?.state + ",  INDIA"}</div>
				</Col>
				{showActions && (
					<Col xs={2} sm={2} md={2} l={2} xl={2}>
						<Row gutter={[12, 12]}>
							<Col span={24}>
								<EditOutlined onClick={onClickEdit} />
							</Col>
							<Col span={24}>
								{/* <DeleteOutlined onClick={onClickDelete}/> */}
							</Col>
						</Row>
					</Col>
				)}
			</Row>
		</AddressCardWrapper>
	)
}

export default CartItem
