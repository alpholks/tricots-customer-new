import ProductCardWrapper from "./ProductCard.style";
import React, { useEffect, useState } from "react";
import { Images } from "./assets";
import { Row, Col , message} from "antd";
import { HeartOutlined, HeartFilled } from "@ant-design/icons";

export function ProductCard(props) {
	const {
		data,
		onClick,
		hideFavorite,
		addToWishList,
		removeFromWishList,
	} = props;

	let token = localStorage.getItem("accessToken");

	return (
		<ProductCardWrapper>
			<div>
				{data?.isNewArrival && <div className="badge">New Arrival</div>}
				{!hideFavorite && data?.isFavourite ? (
					<div className="favorite">
						<HeartFilled
							className="favoriteIcon"
							onClick={() => {
								if (token) removeFromWishList(data._id);
								else message.warning("Please login to make a Wishlist")
							}}
						/>
					</div>
				) : (
					!hideFavorite && (
						<div className="favorite">
							<HeartOutlined
								onClick={() => {
									if (token) addToWishList(data._id);
									else message.warning("Please login to make a Wishlist")
								}}
							/>
						</div>
					)
				)}
				<img
					onClick={onClick}
					src={
						data?.imageId?.imageUrl ? data?.imageId?.imageUrl : Images.product1
					}
					style={{
						width: "100%",
						height: "300px",
						objectFit: "cover",
					}}
				/>
			</div>

			<Row gutter={24} onClick={onClick}>
				<Col xs={24} sm={24} md={24} l={24} xl={24}>
					<div className="product-name">{data?.name}</div>
				</Col>
				<Col xs={24} sm={24} md={24} l={24} xl={24}>
					<div className="product-price">
						<span className="special-price-text">
							₹{data?.specialPrice ? data?.specialPrice : null}
						</span>
						<span className="original-price-text">
							₹{data?.defaultPrice ? data?.defaultPrice : null}
						</span>
					</div>
				</Col>
				<Col xs={24} sm={24} md={24} l={24} xl={24}>
					<div className="offer-text">({data?.offerValue}% off)</div>
				</Col>
			</Row>
		</ProductCardWrapper>
	);
}

export default ProductCard;
