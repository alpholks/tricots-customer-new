import styled from 'styled-components'
import Theme from '../../themes/themes'

const Wrapper = styled.section`
	font-family: "Montserrat", sans-serif;
	margin: 5px;
	cursor: pointer;
	.badge {
		position: absolute;
		text-align: left;
		background: #c32148;
		float: left;
		/* line-height: 18px; */
		color: white;
		margin-top: 0px;
		font-size: 12px;
		padding-left: 5px;
		text-transform: uppercase;
	}
	.favorite {
		position: absolute;
		background: #ffffff;
		padding: 5px;
		width: 25px;
		right: 5px;
		height: 25px;
		margin: 5px;
		display: flex;
		justify-content: center;
		align-items: center;
		border-radius: 50px;
		font-size: 16px;
		text-transform: uppercase;
	}
	.favoriteIcon{
		color : ${Theme.primary};
	}
	.product-name {
		white-space: nowrap;
		overflow: hidden;
		text-overflow: ellipsis;
	}
	.product-price {
		white-space: nowrap;
		overflow: hidden;
		text-overflow: ellipsis;
		display: flex;
		flex-direction: row;
		.original-price-text {
			margin-right: 10px;
			color: ${Theme.negative};
			text-decoration: line-through;
		}
		.special-price-text {
			margin-right: 10px;
			color: ${Theme.primary};
			font-weight: 600;
		}
	}
	.offer-text {
		color: ${Theme.positive};
		font-size: 12px;
	}

`

export default Wrapper
