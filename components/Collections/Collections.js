import CollectionWrapper from "./Collections.style";
import { Images } from "./assets/index";
import React, { useState } from "react";
import { Row, Col } from "antd";

import { LeftCircleFilled, RightCircleFilled } from "@ant-design/icons";

export function Collections(props) {
	const { data, onClickCollection } = props;
	return (
		<CollectionWrapper view="mobile">
			<div className="wrapper">
				<div className="title">COLLECTIONS</div>
				<Row gutter={[8, 8]} justify={"center"} align="middle">
					{/* <Col lg={3} xs={0}>
						<Row gutter={[8, 8]}>
							<Col span={24}>
								<LeftCircleFilled className="direction-icon" />
							</Col>
						</Row>
					</Col> */}
					{/* <Col lg={24} xs={24}>
						<img
							onClick={() =>
								onClickCollection(
									data[0]?.name
										.split(" ")
										.map((word) => word.toLowerCase())
										.join("-")
								)
							}
							src={data[0]?.imageId.imageUrl}
							className="collection"
						/>
					</Col> */}
					<Col lg={24} xs={24}>
						<Row gutter={[10, 10]}>
							{data?.map((collection, index) => {
								return (
									<Col xs={index % 9 == 0? 24 : 12} lg={index % 9 == 0 ? 12 : 6}>
										<img
											onClick={() =>
												onClickCollection(
													collection?.slug
												)
											}
											src={collection.imageId.imageUrl}
											className="collection"
										/>
									</Col>
								);
							})}
						</Row>
					</Col>
					{/* <Col lg={3} xs={0}>
						<Row gutter={[8, 8]}>
							<Col span={24}>
								<RightCircleFilled className="direction-icon" />
							</Col>
						</Row>
					</Col> */}
				</Row>
			</div>
		</CollectionWrapper>
	);
}

export default Collections;
