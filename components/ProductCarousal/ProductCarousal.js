import ProductCarousalWrapper from "./ProductCarousal.style";
import React, { useEffect, useState } from "react";
import { Images } from "./assets";
import { Row, Col, Grid, Button, Modal, Select, message } from "antd";
import {
	HeartOutlined,
	HeartFilled,
	PlusCircleFilled,
	MinusCircleFilled,
	PlusSquareFilled,
	MinusSquareFilled,
	ShoppingCartOutlined,
	EyeOutlined,
	PaperClipOutlined,
	ShopFilled,
	ArrowRightOutlined,
	TagFilled,
} from "@ant-design/icons";
import axios from "axios";
import { API_URL } from "../../constants";
import { useRouter } from "next/router";
import { Collapse } from "antd";
import { useSelector } from "react-redux";

const { useBreakpoint } = Grid;
const { Option } = Select;

const { Panel } = Collapse;
const text = "sdcsd";
export function ProductCarousal(props) {
	const { comboCart } = useSelector((state) => state.comboCart);
	const router = useRouter();
	const {
		data,
		onClick,
		onClickPlus,
		onClickMinus,
		cart: cartD,
		onClickViewCart,
		addToWishList,
		removeFromWishList,
	} = props;
	const { cart } = cartD;
	const screens = useBreakpoint();
	const [colors, setColors] = useState([]);
	const [sizes, setSizes] = useState([]);
	const [pictures, setPictures] = useState([]);
	const [selectedColor, setSelectedColor] = useState("");
	const [selectedInventory, setSelectedInventory] = useState("");
	const [selectedPicture, setSelectedPicture] = useState("");
	const [quantity, setQuantity] = useState(1);
	const [showPopup, setPopup] = useState(false);
	const [charts, setCharts] = useState([]);
	const [promocodes, setPromocodes] = useState();
	const [admin, setAdmin] = useState();
	let token = localStorage.getItem("accessToken");

	const callback = () => {};
	const getChart = () => {
		axios
			.get(API_URL + "/guest/charts", {
				headers: {
					Authorization: `JWT ${localStorage.getItem("accessToken")}`,
				},
			})
			.then((res) => {
				setCharts(res.data.data);
			})
			.catch((err) => {
				alert(err.message);
			});
	};

	useEffect(() => {
		if (data?.inventories) {
			setPictures(
				data?.inventories?.map((i) => {
					return i?.imageId?.imageUrl;
				})
			);

			let sizeList = [];
			data?.inventories?.map((i) => {
				return !sizeList.includes(i.size) && sizeList.push(i.size);
			});
			setSizes(sizeList);
			let colorList = [];

			data?.inventories?.map((i) => {
				return !colorList.includes(i.color) && colorList.push(i.color);
			});
			setColors(colorList);
			setSelectedColor(colorList ? colorList[0] : "");
			handleSelectInventory(
				colorList ? colorList[0] : "",
				sizeList ? sizeList[0] : ""
			);
			setSelectedPicture(data?.imageId?.imageUrl);
		}
		getChart();
		getPromocodes();
		getAdmin();
	}, [data?.inventories]);

	useEffect(() => {
		if (selectedInventory) {
			if (selectedInventory && cart && cart[selectedInventory._id]) {
				setQuantity(cart && cart[selectedInventory._id].quantity);
			} else {
				setQuantity(1);
			}
		}
	}, [cartD, selectedInventory]);

	const handleSelectInventory = (color, size) => {
		var inData = data?.inventories?.filter((item) => {
			if (item.color === color && item.size === size) return item;
		});
		if (inData.length != 0) {
			setSelectedInventory(inData[0]);
		}
	};

	const getPromocodes = () => {
		axios
			.get(`${API_URL}/guest/promocodes`, {
				headers: {
					Authorization: `JWT ${localStorage.getItem("accessToken")}`,
				},
			})
			.then((res) => {
				setPromocodes(res.data.data);
			});
	};
	const getAdmin = () => {
		axios
			.get(`${API_URL}/guest/admins`, {
				headers: {
					Authorization: `JWT ${localStorage.getItem("accessToken")}`,
				},
			})
			.then((res) => {
				setAdmin(res.data.data);
			});
	};

	useEffect(() => {
		console.log(promocodes, admin);
	}, [promocodes, admin]);

	return (
		<ProductCarousalWrapper>
			<Modal
				title="Basic Modal"
				visible={showPopup}
				onOk={() => setPopup(false)}
				onCancel={() => setPopup(false)}
			>
				<img
					src={charts?.[0]?.imageId?.imageUrl}
					style={{ width: "100%", height: "100%" }}
				/>
				<Row gutter={[10, 24]} className="carousal-container" justify="start">
					<Col span={6}>Size</Col>
					<Col span={6}>Chest</Col>
					<Col span={6}>Length</Col>
					<Col span={6}>Sleeve Length</Col>
				</Row>
				{charts?.[0]?.details?.map((row) => {
					return (
						<Row
							gutter={[10, 24]}
							className="carousal-container"
							justify="start"
						>
							<Col span={6}>{row.size}</Col>
							<Col span={6}>{row.chest}</Col>
							<Col span={6}>{row.length}</Col>
							<Col span={6}>{row.sleeveLength}</Col>
						</Row>
					);
				})}
			</Modal>
			<Row className={screens.xs ? "product-view" : ""} justify="space-between">
				<Col xs={24} lg={12}>
					{data?.isNewArrival && <div className="badge">New Arrival</div>}
					{data?.isFavourite ? (
						<div className="favorite">
							<HeartFilled
								className="favoriteIcon"
								onClick={() => {
									if (token) removeFromWishList(data._id);
									else message.warning("Please login to make a Wishlist");
								}}
							/>
						</div>
					) : (
						<div className="favorite">
							<HeartOutlined
								onClick={() => {
									if (token) addToWishList(data._id);
									else message.warning("Please login to make a Wishlist");
								}}
							/>
						</div>
					)}
					{!screens.xs && (
						<img
							onClick={onClick}
							src={selectedPicture ? selectedPicture : Images.product1}
							className="fullImage"
						/>
					)}
					{screens.xs && (
						<img
							src={selectedPicture ? selectedPicture : Images.product1}
							style={{ width: "100%", height: "60%", objectFit: "cover" }}
						/>
					)}
					<Row
						gutter={[10, 24]}
						className="carousal-container"
						justify="center"
						style={{}}
					>
						{screens.xs ? (
							<div
								style={{
									overflow: "auto",
									whiteSpace: "nowrap",
							
								}}
							>
								{[...pictures, data?.imageId?.imageUrl]?.map((p) => {
									return (
										<img
											onClick={() => setSelectedPicture(p)}
											src={p}
											style={{
												padding: 5,
												width: 100,
												height: 100,
												objectFit: "contain",
											}}
											// className="smallImage1"
										/>
									);
								})}
							</div>
						) : (
							[...pictures, data?.imageId?.imageUrl]?.map((p) => {
								return (
									<Col span={8}>
										<img
											onClick={() => setSelectedPicture(p)}
											src={p}
											className="smallImage"
										/>
									</Col>
								);
							})
						)}
					</Row>
				</Col>
				<Col xs={24} lg={11} style={{marginTop :screens.xs ?  -100 : -20}}>
					<Row gutter={0}>
						<Col xs={24} sm={24} md={24} l={24} xl={24}>
							<div className="product-name">{data?.name}</div>
						</Col>
						<Col xs={24} sm={24} md={24} l={24} xl={24}>
							<div className="product-price">
								<span className="special-price-text">
									₹{data?.specialPrice}
								</span>
								<span className="original-price-text">
									₹{data?.defaultPrice}
								</span>
							</div>
						</Col>
						<Col xs={24} sm={24} md={24} l={24} xl={24}>
							<div className="offer-text">({data?.offerValue}% off)</div>
						</Col>
						<Col xs={24} sm={24} md={24} l={24} xl={24}>
							<div className="sub-text">
								Inclusive Of All Taxes +{" "}
								<span className="special-price-text">
									{/* {JSON.parse(localStorage.getItem("admin"))?.deliveryCharge
										? "₹ " +
										  JSON.parse(localStorage.getItem("admin")).deliveryCharge
										: "Free Shipping"} */}
										Free Shipping
								</span>
							</div>
						</Col>
						<Col span={24}>
							<Collapse onChange={callback} style={{ marginTop: 20 }}>
								<Panel header="Promocodes" key="1">
									{promocodes?.map((code) => {
										return (
											<div style={{ display: "flex", flexDirection: "row" }}>
												<TagFilled style={{ margin: 5 }} /> {code?.name}
											</div>
										);
									})}
								</Panel>
							</Collapse>
						</Col>
					</Row>

					<Row gutter={12}>
						<Col xs={24} sm={24} md={24} l={24} xl={24}>
							<div className="product-name">Color</div>
						</Col>
						{colors.map((color) => {
							return (
								<Col>
									<div
										onClick={() => setSelectedColor(color)}
										className={
											color === selectedColor
												? "selectedColorCircle"
												: "colorCircle"
										}
										style={{
											backgroundColor: color,
										}}
									></div>
								</Col>
							);
						})}
					</Row>
					<Row gutter={12}>
						<Col xs={24} sm={24} md={24} l={24} xl={24}>
							{selectedColor && <div className="product-name">Size</div>}
						</Col>
						{sizes.map((size) => {
							var condition = data?.inventories?.filter((item) => {
								if (item.color === selectedColor) return item;
							});
							if (condition.length !== 0)
								return (
									<Col>
										<div
											onClick={() => handleSelectInventory(selectedColor, size)}
											className={
												selectedInventory.size === size
													? "selectedSizeBox"
													: "sizeBox"
											}
										>
											{size}
										</div>
									</Col>
								);
						})}
					</Row>
					<Row gutter={12} justify={screens.xs ? "space-between" : "start"}>
						<Col xs={24} sm={24} md={24} l={24} xl={24}>
							<div className="product-name">Quantity</div>
						</Col>
						<Col>
							<Select
								defaultValue={quantity}
								value={quantity}
								style={{ width: 150 }}
								onChange={(value) => setQuantity(value)}
							>
								{[1, 2, 3, 4, 5, 6, 7, 8, 9, 10].map((q) => {
									return <Option value={q}>{q}</Option>;
								})}
							</Select>
						</Col>
						<Col>
							<Button
								icon={<ShoppingCartOutlined />}
								type="primary"
								onClick={() => onClickPlus(selectedInventory, quantity)}
							>
								Add To Cart
							</Button>
						</Col>
						{!screens.xs && (
							<Col>
								<Button
									icon={<ShoppingCartOutlined />}
									type={"primary"}
									onClick={onClickViewCart}
								>
									View Cart
								</Button>
							</Col>
						)}
						<Col
							xs={screens.xs && 24}
							style={{ marginTop: screens.xs ? "15px" : "0px" }}
						>
							<Button
								style={{ width: "100%" }}
								type=""
								onClick={() => setPopup(true)}
							>
								Size Chart
							</Button>
						</Col>
					</Row>
					<Row gutter={12} justify="space-between">
						{/* <Col xs={12} sm={12} md={12} l={12} xl={12}>
							{selectedInventory ? (
								<Button
									onClick={() => {
										onClickPlus(selectedInventory);
									}}
									type="primary"
									style={{ marginTop: 20 }}
								>
									Add To Cart
								</Button>
							) : (
								<Row gutter={0} align="middle">
									<Col style={{ marginTop: 20 }}>
										<MinusSquareFilled
											className="cartText"
											onClick={() =>
												onClickMinus(selectedInventory)
											}
										/>
									</Col>
									<Col style={{ marginTop: 20 }}>
										<div className="cartText">
											{quantity}
										</div>
									</Col>
									<Col style={{ marginTop: 20 }}>
										<PlusSquareFilled
											className="cartText"
											onClick={() =>
												onClickPlus(selectedInventory)
											}
										/>
									</Col>
								</Row>
							)}
						</Col>
						<Col
							style={{ marginTop: 20 }}
							xs={12}
							sm={12}
							md={12}
							l={12}
							xl={12}
						>
							<Button type="" onClick={onClickViewCart}>
								View Cart
							</Button>
						</Col> */}
					</Row>
					{/* <Col xs={12} sm={12} md={24} l={24} xl={24}>
						<Button type="" onClick={() => setPopup(true)}>
							size chart
						</Button>
					</Col> */}
					<Row gutter={0} className="product-common-data">
						<Col xs={24} sm={24} md={24} l={24} xl={24}>
							<li>Cash on Delivery Available</li>
							<li>Vocal for Local -100% Swadeshi Product</li>
							<li>Packed with safety</li>
						</Col>
					</Row>

					<Row gutter={0} className="product-common-data">
						{data?.description}
						{/* <Col xs={24} sm={24} md={24} l={24} xl={24}>
							<div className="product-name">Style</div>
						</Col>
						<Col>
							<div>
								Regular Fit: Fits just right - not too tight,
								not too loose.
							</div>
						</Col>
						<Col xs={24} sm={24} md={24} l={24} xl={24}>
							<div className="product-name">Fabric</div>
						</Col>
						<Col>
							<div>100% Cotton Single Jersey and bio washed.</div>
						</Col>
						<Col xs={24} sm={24} md={24} l={24} xl={24}>
							<div className="product-name">Design Brief</div>
						</Col>
						<Col>
							<div>
								The savior of Gotham City is now slaying the
								attire with an amazing appearance. Wear the
								t-shirt as a cape and rule the fashion with the
								batman t-shirt.
							</div>
						</Col>
						<Col xs={24} sm={24} md={24} l={24} xl={24}>
							<div className="product-name">
								Product Description
							</div>
						</Col>
						<Col>
							<div>
								“It’s not who I am underneath, but what I do
								that defines me” these are the lines by batman.
								Batman is a DC comic character popular for its
								bat-like look. Batarang sign got popular after
								the movie Batman because the sign was marked on
								the suit of batman. It has a very dark and deep
								theme, which separates it from other logos. If
								you are a true Batman fan, order this Batarang
								half sleeve t-shirt for men and have a clear
								night vision like a bat.
							</div>
						</Col>
						<Col xs={24} sm={24} md={24} l={24} xl={24}>
							<div className="product-name">
								Delivery & Return Policy
							</div>
						</Col>
						<Col>
							<div>
								Pay online & get free shipping. Cash Collection
								Charges applicable. Please, refer FAQ for more
								information. All products are applicable for
								return. Customers can return their order within
								15 days of the order delivery. Refunds for
								returned products will be given in your
								Respective Account.
							</div>
						</Col> */}
					</Row>
				</Col>
			</Row>
			{screens.xs &&
			(Object.keys(cart).length !== 0 ||
				Object.keys(comboCart).length !== 0) ? (
				<Button
					type={"primary"}
					icon={<EyeOutlined />}
					className="add-to-cart-btn"
					onClick={() => router.push("/cart")}
				>
					View Cart ({Object.keys(cart).length + Object.keys(comboCart).length})
				</Button>
			) : null}

			<div className="product-name2">SIMILIAR PRODUCTS</div>
			{/* <Row gutter={12} justify="center" align="middle">
				<Col xs={8} sm={8} md={8} l={8} xl={8}>
					<div className="quality-card">
						<img
							src={Images.product4}
							style={{ width: "100%", objectFit: "cover" }}
						/>
						<p className="sub-text">Home Grown Brand</p>
					</div>
				</Col>
				<Col xs={8} sm={8} md={8} l={8} xl={8}>
					<div className="quality-card">
						<img
							src={Images.product4}
							style={{ width: "100%", objectFit: "cover" }}
						/>
						<p className="sub-text">100% Quality Assured</p>
					</div>
				</Col>
				<Col xs={8} sm={8} md={8} l={8} xl={8}>
					<div className="quality-card">
						<img
							src={Images.product4}
							style={{ width: "100%", objectFit: "cover" }}
						/>
						<p className="sub-text">Secure Payments</p>
					</div>
				</Col>
			</Row> */}
		</ProductCarousalWrapper>
	);
}

export default ProductCarousal;
