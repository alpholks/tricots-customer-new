import ComboPackWrapper from "./ComboPack.style";
import React, { useState, useEffect } from "react";
import { Images } from "./assets";
import { Row, Col, Form, Input, Button, Modal, Select, Grid } from "antd";
import {
	HeartOutlined,
	MinusCircleOutlined,
	PlusOutlined,
	ShopFilled,
	ShoppingCartOutlined,
	EyeOutlined,
} from "@ant-design/icons";
import axios from "axios";
import ComboSelector from "../ComboSelector";
import { useRouter } from "next/router";
import { API_URL } from "../../constants";
import { useSelector } from "react-redux";

const { useBreakpoint } = Grid;

const { Option } = Select;
export function ComboPack(props) {
	const { cart } = useSelector((state) => state.cart);

	const {
		data,
		onClick,
		onClickPlus,
		onClickMinus,
		cart: cartD,
		onClickViewCart,
	} = props;
	const { comboCart } = cartD;
	const router = useRouter();
	const screens = useBreakpoint();
	const [colors, setColors] = useState([]);
	const [sizes, setSizes] = useState([]);
	const [pictures, setPictures] = useState([]);
	const [number, setNumber] = useState([]);
	const [selectedColor, setSelectedColor] = useState("");
	const [selectedInventory, setSelectedInventory] = useState("");
	const [selectedPicture, setSelectedPicture] = useState("");
	const [quantity, setQuantity] = useState(0);
	const [showPopup, setPopup] = useState(false);
	const [charts, setCharts] = useState([]);

	const getChart = () => {
		axios
			.get(API_URL + "/guest/charts", {
				headers: {
					Authorization: `JWT ${localStorage.getItem("accessToken")}`,
				},
			})
			.then((res) => {
				setCharts(res.data.data);
			})
			.catch((err) => {
				alert(err.message);
			});
	};

	const setData = async () => {
		if (data?.inventories) {
			setPictures(
				data?.inventories?.map((i) => {
					return i?.imageId?.imageUrl;
				})
			);

			var sizeList = [];
			await Promise.all(
				data?.inventories?.map((i) => {
					if (!sizeList.includes(i.size) && selectedColor === i.color) {
						return sizeList.push(i.size);
					}
				})
			);
			setSizes(sizeList);
			var colorList = [];
			console.log(data.inventories);
			await Promise.all(
				data?.inventories?.map((i) => {
					if (!colorList.includes(i.color)) {
						return colorList.push({color : i.color, colorName : i.colorName});
					}
				})
			);
			setColors(colorList);
			var numberList = [];
			for (var i = 0; i < data?.numberOfItems; i++) {
				numberList.push(i);
			}
			setNumber(numberList);
			setSelectedPicture(data?.imageId?.imageUrl);
		}
		getChart();
	};
	useEffect(() => {
		setData();
	}, [data?.inventories, selectedColor]);

	useEffect(() => {
		if (selectedInventory && comboCart && comboCart[selectedInventory._id]) {
			setQuantity(comboCart && comboCart[selectedInventory._id].quantity);
		} else {
			setQuantity(0);
		}
	}, [cartD, selectedInventory]);

	const handleSelectInventory = (color, size, key) => {
		var inData = data?.inventories?.filter((item) => {
			if (item.color === color && item.size === size) return item;
		});
		if (inData.length != 0) {
			setSelectedInventory(inData[0]);
			onClickPlus(inData[0]);
		}
	};

	return (
		<ComboPackWrapper>
			<Modal
				title="Basic Modal"
				visible={showPopup}
				onOk={() => setPopup(false)}
				onCancel={() => setPopup(false)}
			>
				<img
					src={charts?.[0]?.imageId?.imageUrl}
					style={{ width: "100%", height: "100%" }}
				/>
				<Row gutter={[10, 24]} className="carousal-container" justify="start">
					<Col span={6}>Size</Col>
					<Col span={6}>Chest</Col>
					<Col span={6}>Length</Col>
					<Col span={6}>Sleeve Length</Col>
				</Row>
				{charts?.[0]?.details?.map((row) => {
					return (
						<Row
							gutter={[10, 24]}
							className="carousal-container"
							justify="start"
						>
							<Col span={6}>{row.size}</Col>
							<Col span={6}>{row.chest}</Col>
							<Col span={6}>{row.length}</Col>
							<Col span={6}>{row.sleeveLength}</Col>
						</Row>
					);
				})}
			</Modal>
			<Row className={screens.xs ? "product-view" : ""} justify="space-between">
				<Col xs={24} lg={12}>
					{data?.isNewArrival && <div className="badge">New Arrival</div>}
				{!screens.xs && (
						<img
							onClick={onClick}
							src={
								selectedPicture
									? selectedPicture
									: Images.product1
							}
							className="fullImage"
						/>
					)}
					{screens.xs && (
						<img
							src={
								selectedPicture
									? selectedPicture
									: Images.product1
							}
							style={{ width: "100%", height :"60%", objectFit: "cover" }}
						/>
					)}
					<Row gutter={[10, 24]} className="carousal-container" justify="start">
						{screens.xs ? (
							<div
								style={{
									overflow: "auto",
									whiteSpace: "nowrap",
								}}
							>
								{[...pictures, data?.imageId?.imageUrl]?.map((p) => {
									return (
										<img
											onClick={() => setSelectedPicture(p)}
											src={p}
											style={{
												padding: 5,
												width: 100,
												height: 100,
												objectFit: "contain",
											}}
											className="smallImage"
										/>
									);
								})}
							</div>
						) : (
							[...pictures, data?.imageId?.imageUrl]?.map((p) => {
								return (
									<Col span={8}>
										<img
											onClick={() => setSelectedPicture(p)}
											src={p}
											className="smallImage"
										/>
									</Col>
								);
							})
						)}
					</Row>
				</Col>
				<Col xs={24} lg={11} style={{marginTop :screens.xs ?  -100 : -20}}>
					<Row gutter={0}>
						<Col xs={24} sm={24} md={24} l={24} xl={24}>
							<div className="product-name">{data?.name}</div>
						</Col>
						<Col xs={24} sm={24} md={24} l={24} xl={24}>
							<div className="product-price">
								<span className="special-price-text">
									₹{data?.specialPrice}
								</span>
								<span className="original-price-text">
									₹{data?.defaultPrice}
								</span>
							</div>
						</Col>
						<Col xs={24} sm={24} md={24} l={24} xl={24}>
							<div className="offer-text">({data?.offerValue}% off)</div>
						</Col>
						<Col xs={24} sm={24} md={24} l={24} xl={24}>
							<div className="sub-text">
								Inclusive Of All Taxes +{" "}
								<span className="special-price-text">Free Shipping</span>
								{/* <span className="special-price-text">{JSON.parse(localStorage.getItem("admin"))?.deliveryCharge ?"₹ " +JSON.parse(localStorage.getItem("admin")).deliveryCharge : "Free Shipping" }</span> */}
							</div>
						</Col>
					</Row>

					<div className="product-name">Select Color And Sizes</div>

					{number.map((selector, index) => {
						return (
							<ComboSelector
								data={data?.inventories}
								key={index + 1}
								colors={colors}
								sizes={sizes.map((size) => {
									var condition = data?.inventories?.filter((item) => {
										if (item.color === selectedColor) return item;
									});
									if (condition.length !== 0) return size;
								})}
								handleChangeSize={(color, size) =>
									handleSelectInventory(color, size, index)
								}
								handleChangeColor={(color) => setSelectedColor(color)}
							/>
						);
					})}

					<Row
						gutter={0}
						className="product-common-data"
						justify="space-between"
					>
						<Button
							className="checkout-btn"
							type="primary"
							onClick={() => {
								router.push("/checkout");
							}}
							icon={<ShoppingCartOutlined />}
							onClick={props.onClickAddToCart}
						>
							Add To Cart
						</Button>
						<Button type="" onClick={() => setPopup(true)}>
							size chart
						</Button>
					</Row>

					<Row gutter={0} className="product-common-data">
						<Col xs={24} sm={24} md={24} l={24} xl={24}>
							<li>Cash on Delivery Available</li>
							<li>Vocal for Local -100% Swadeshi Product</li>
							<li>Packed with safety</li>
						</Col>
					</Row>
					<Row gutter={0} className="product-common-data">
						{data?.description}
						{/* <Col xs={24} sm={24} md={24} l={24} xl={24}>
							<div className="product-name">Style</div>
						</Col>
						<Col>
							<div>
								Regular Fit: Fits just right - not too tight,
								not too loose.
							</div>
						</Col>
						<Col xs={24} sm={24} md={24} l={24} xl={24}>
							<div className="product-name">Fabric</div>
						</Col>
						<Col>
							<div>100% Cotton Single Jersey and bio washed.</div>
						</Col>
						<Col xs={24} sm={24} md={24} l={24} xl={24}>
							<div className="product-name">Design Brief</div>
						</Col>
						<Col>
							<div>
								The savior of Gotham City is now slaying the
								attire with an amazing appearance. Wear the
								t-shirt as a cape and rule the fashion with the
								batman t-shirt.
							</div>
						</Col>
						<Col xs={24} sm={24} md={24} l={24} xl={24}>
							<div className="product-name">
								Product Description
							</div>
						</Col>
						<Col>
							<div>
								“It’s not who I am underneath, but what I do
								that defines me” these are the lines by batman.
								Batman is a DC comic character popular for its
								bat-like look. Batarang sign got popular after
								the movie Batman because the sign was marked on
								the suit of batman. It has a very dark and deep
								theme, which separates it from other logos. If
								you are a true Batman fan, order this Batarang
								half sleeve t-shirt for men and have a clear
								night vision like a bat.
							</div>
						</Col>
						<Col xs={24} sm={24} md={24} l={24} xl={24}>
							<div className="product-name">
								Delivery & Return Policy
							</div>
						</Col>
						<Col>
							<div>
								Pay online & get free shipping. Cash Collection
								Charges applicable. Please, refer FAQ for more
								information. All products are applicable for
								return. Customers can return their order within
								15 days of the order delivery. Refunds for
								returned products will be given in your
								Respective Account.
							</div>
						</Col> */}
					</Row>
				</Col>
			</Row>
			{screens.xs &&( Object.keys(cart).length !== 0 ||Object.keys(comboCart).length  !== 0 ) ? (
				<Button
					type={"primary"}
					icon={<EyeOutlined />}
					className="add-to-cart-btn"
					onClick={() => router.push("/cart")}
				>
					View Cart ({Object.keys(cart).length +Object.keys(comboCart).length })
				</Button>
			) : null}
			<div className="product-name">SIMILIAR PRODUCTS</div>
		</ComboPackWrapper>
	);
}

export default ComboPack;
