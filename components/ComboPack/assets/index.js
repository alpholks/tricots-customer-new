const Images = {
    banner1: require('./images/banner_2.jpg'),
    banner2: require('./images/top-small-banner.jpg'),
    banner4: require('./images/banner-3.jpg'),
    banner5: require('./images/banner-4.jpg'),
    banner3: require('./images/banner-5.jpg'),
    product1: require('./images/product1.jpg'),
    product2: require('./images/product2.jpg'),
    product4: require('./images/product3.jpg'),
    product5: require('./images/product4.jpg'),
    product3: require('./images/product5.jpg')
}
export {
	Images
}
const ComponentAssets = {
	Images
}
export default ComponentAssets
