const Images = {
	logo: require('./images/logo.jpg'),
	shipping: require('./images/shipping.webp'),
	Payments : require("./images/payments.png")
}
export {
	Images
}
const ComponentAssets = {
	Images
}
export default ComponentAssets
