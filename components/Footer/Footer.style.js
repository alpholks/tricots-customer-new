import styled from 'styled-components'
import Theme from '../../themes/themes'

const Wrapper = styled.section`
	font-family: "Montserrat", sans-serif;
	width: 100vw;
	width: 100%;
	.footer-container {
	}
	.footer {
		/* width: 100vw; */
		padding : 20px;
		background-color: #f5f5f5;
		height: 100%;
	}
	.sub-footer {
		font-family: "Montserrat", sans-serif;
		font-size: 10px;
		display: flex;
		justify-content: center;
		align-items: center;
		padding: 10px;
	}
	.sub-footer2{
		font-family: "Montserrat", sans-serif;
		font-size: 10px;
		display: ${props => props.sm ?  "flex": "auto" };
		justify-content: space-around;
		align-items: center;
		padding: 10px;
		background-color :#800000;
	}
	.footer-menu-heading {
		font-family: "Montserrat", sans-serif;
		font-weight: bolder;
		cursor: pointer;
	}
	[data-theme="compact"]
		.site-collapse-custom-collapse
		.site-collapse-custom-panel,
	.site-collapse-custom-collapse .site-collapse-custom-panel {
		/* margin-bottom: 24px; */
		overflow: hidden;
		background: #f7f7f7;
		border: 0px;
		border-radius: 2px;
	}
`

export default Wrapper
