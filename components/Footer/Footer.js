import React, { useState, useEffect } from "react";
import { Row, Col, Grid, Collapse, Input, Button, message } from "antd";

import {
	PlusOutlined,
	MinusOutlined,
	YoutubeFilled,
	InstagramFilled,
	WhatsAppOutlined,
	FacebookFilled,
} from "@ant-design/icons";
import Link from "next/link";
import FooterWrapper from "./Footer.style";
import { Images } from "./assets/index";
import { useRouter } from "next/router";
import axios from "axios";
import { API_URL } from "../../constants";
const { Panel } = Collapse;
const { useBreakpoint } = Grid;

export default function Footer(props) {
	const screens = useBreakpoint();
	const [admin, setAdmin] = useState();
	const [email, setEmail] = useState();
	const [messageText, setMessage] = useState();
	const router = useRouter();

	const getAdmin = () => {
		let admin = JSON.parse(localStorage.getItem("admin"));
		setAdmin(admin);
	};
	useEffect(() => {
		getAdmin();
	}, []);

	const onSubmit = () => {
		axios
			.post(
				`${API_URL}/guest/subscribe`,
				{ email: email, message: messageText },
				{
					headers: {
						Authorization: `JWT ${localStorage.getItem("accessToken")}`,
					},
				}
			)
			.then((res) => {
				message.success("Subscribed");
			});
	};

	const footerData = [
		{
			name: "LOCATION",
			dataRow: [
				admin?.address,
				// '1-C-12, Gayatri Nagar, Sector 5',
				// 'Udaipur, India 313002 '
			],
		},
		{
			name: "COMPANY",
			dataRow: [
				{ component: "About Us", link: "/about-us" },
				// 'Beyoung Blog',
				// 'Beyoungistan',
				{ component: "Term and Conditions", link: "/terms-and-conditions" },
				{ component: "Privacy Policy", link: "/privacy-policy" },
				//{component:// "Return Policy", link  : "/" },
				{ component: "Shipping Policy", link: "/shipping-policy" },
			],
		},
		{
			name: "NEED HELP",
			dataRow: [
				// 'Contact Us',
				// 'Return, Refund and Cancellation',
				// "FAQ's",
				{ component: "Track Order", link: "/track-order" },
				{ component: "Return, Refund and Cancellation Policy", link: "/return-and-refund-policy" },
				{ component: "Contact Us", link: "/contact-us" },
				// "Track Order",
				// 'Career',
				// 'Sitemap'
			],
		},
		{
			name: "LETS BE FRIENDS",
			dataRow: [
				{
					component: (
						<span>
							<YoutubeFilled style={{ color: "red" }} /> Youtube
						</span>
					),
					link: admin?.youtube,
				},
				<span>
					<WhatsAppOutlined style={{ color: "green" }} /> {admin?.whatsapp}
				</span>,
				{
					component: (
						<span>
							<FacebookFilled style={{ color: "blue" }} /> Facebook
						</span>
					),
					link: admin?.facebook,
				},
				{
					component: (
						<span>
							<InstagramFilled style={{ color: "purple" }} /> Instagram
						</span>
					),
					link: admin?.instagram,
				},
			],
		},
	];

	return (
		<FooterWrapper sm={screens.sm}>
			<div className="footer-container">
				<div className="footer">
					{!screens.xs && (
						<Row justify="center" gutter={[24, 12]}>
							{footerData.map((column, index) => {
								return (
									<Col lg={5} xs={12}>
										<Row gutter={[24, 12]}>
											<Col xs={24} className="footer-menu-heading">
												{column.name}
											</Col>
											{column.dataRow.map((row) => {
												if (row?.component) {
													return (
														<Link  href={row.link ? row.link : ""} passHref>
															<Col style={{cursor:"pointer"}} xs={24}>{row?.component}</Col>
														</Link>
													);
												} else return <Col xs={24}>{row}</Col>;
											})}
											{/* <Col xs={24}>
									1-C-12, Gayatri Nagar, Sector 5
								</Col>
								<Col xs={24}>Udaipur, India 313002</Col> */}
										</Row>
									</Col>
								);
							})}
						</Row>
					)}
					{screens.xs && (
						<Collapse
							bordered={false}
							defaultActiveKey={[]}
							expandIconPosition={"right"}
							expandIcon={({ isActive }) =>
								isActive ? (
									<MinusOutlined rotate={0} />
								) : (
									// <CaretRightOutlined rotate={isActive ? 90 : 0} />
									<PlusOutlined rotate={0} />
								)
							}
							className="site-collapse-custom-collapse"
						>
							{footerData.map((column, index) => {
								return (
									<Panel
										header={column.name}
										key={index}
										className="site-collapse-custom-panel"
									>
										<Row gutter={[24, 12]}>
											{column.dataRow.map((row) => {
												if (row?.component) {
													return (
														<Link href={row.link ? row.link : ""} passHref>
															<Col xs={24}>{row?.component}</Col>
														</Link>
													);
												} else return <Col xs={24}>{row}</Col>;
											})}
										</Row>
									</Panel>
								);
							})}

							{/* <Panel
							header="This is panel header 2"
							key="2"
							className="site-collapse-custom-panel"
						>
							<p>{text}</p>
						</Panel>
						<Panel
							header="This is panel header 3"
							key="3"
							className="site-collapse-custom-panel"
						>
							<p>{text}</p>
						</Panel> */}
						</Collapse>
					)}
				</div>
				<div className="sub-footer2">
					<Input
						onChange={(e) => setEmail(e.target.value)}
						style={{
							margin: screens.sm ? 10 : 0,
							width: "100%",
							marginTop: 10,
							backgroundColor: "#800000",
							color: "#fff",
						}}
						placeholder={"Email Address"}
					/>
					<Input
						onChange={(e) => setMessage(e.target.value)}
						style={{
							margin: screens.sm ? 10 : 0,
							width: "100%",
							marginTop: 10,
							backgroundColor: "#800000",
							color: "#fff",
						}}
						placeholder={"Message"}
					/>

					<Button
						style={{
							margin: screens.sm ? 10 : 0,
							width: "100%",
							marginTop: 10,
							color: "#800000",
						}}
						onClick={() => onSubmit()}
					>
						Subscribe
					</Button>
				</div>
				<div className="sub-footer">
					<img src={Images.shipping} width={"50%"} />
				</div>
				<div className="sub-footer">
					<img src={Images.Payments} width={"50%"} />
				</div>

				<div className="sub-footer">
					Copyright © 2021 Lukantake. All rights reserved.
				</div>
			</div>
		</FooterWrapper>
	);
}
