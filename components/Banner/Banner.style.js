import styled from 'styled-components'
import Theme from '../../themes/themes'

const Wrapper = styled.section`
	font-family: "Montserrat", sans-serif;
	width: 100vw;
	width: 100%;
	cursor: pointer;
	.banner-1 {
		width: 100%;
		height : ${props => props.view === 'mobile' ? '50px' : '100%'};
		object-fit : ${props => props.view === 'mobile' ? "fill" : "fill"};
	}
	.banner-2 {
		width: 100%;
		height : ${props => props.view === 'mobile' ? '230px' : '100%'};
		object-fit : ${props => props.view === 'mobile' ? "fill" : "fill"};
	}
`

export default Wrapper
