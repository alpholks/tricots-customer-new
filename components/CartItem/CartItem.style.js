import styled from "styled-components";
import Theme from "../../themes/themes";

const Wrapper = styled.section`
	font-family: "Montserrat", sans-serif;
	display: flex;
	flex-direction: column;
	justify-content: center;
	align-items: center;
	margin-bottom: 10px;
	.badge {
		position: absolute;
		text-align: left;
		background: #c32148;
		float: left;
		/* line-height: 18px; */
		color: white;
		margin-top: 0px;
		font-size: 12px;
		padding-left: 5px;
		text-transform: uppercase;
	}
	.cartText {
		font-size: 16px;
		color: ${Theme.primary};
	}
	.favorite {
		position: absolute;
		background: #ffffff;
		padding: 5px;
		width: 25px;
		right: 5px;
		height: 25px;
		margin: 5px;
		display: flex;
		justify-content: center;
		align-items: center;
		border-radius: 50px;
		font-size: 16px;
		text-transform: uppercase;
	}
	.product-name {
		white-space: nowrap;
		overflow: hidden;
		text-overflow: ellipsis;
		font-size: 14px;
		font-weight: 600;
	}
		.product-color {
		height: 10px;
		width : 10px;
		border-radius :10px;
		margin-left:20px;
	}
	.product-price {
		white-space: nowrap;
		overflow: hidden;
		text-overflow: ellipsis;
		display: flex;
		flex-direction: row;
		align-items:center;
		.original-price-text {
			margin-right: 10px;
			color: ${Theme.negative};
			text-decoration: line-through;
		}
		.special-price-text {
			margin-right: 10px;
			color: ${Theme.primary};
			font-weight: 600;
		}
	}
	.offer-text {
		color: ${Theme.positive};
		font-size: 12px;
	}
	.count-minus {
		height: 20px;
		width: 20px;
		border: 1px solid #efefef;
		border-radius: 5px;
		display: flex;
		justify-content: center;
		align-items: center;
	}
	.count-data {
		height: 20px;
		width: 20px;
		display: flex;
		justify-content: center;
		align-items: center;
	}
`;

export default Wrapper;
